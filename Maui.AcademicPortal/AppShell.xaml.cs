﻿using Plugin.LocalNotification;

namespace Maui.AcademicPortal
{
    public partial class AppShell : Shell
    {
        public AppShell()
        {
            InitializeComponent();
            EventSubscriptions();
        }

        public void Dispose()
        {
            this.Loaded -= AppShell_Loaded;
        }

        private void EventSubscriptions()
        {
            this.Loaded += AppShell_Loaded;
        }

        private async void AppShell_Loaded(object? sender, EventArgs e)
        {
            if (await LocalNotificationCenter.Current.AreNotificationsEnabled() == false)
            {
                await LocalNotificationCenter.Current.RequestNotificationPermission();
            }
        }
    }
}
