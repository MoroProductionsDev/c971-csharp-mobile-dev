﻿using Maui.AcademicPortal.MVVM.Models;
using Maui.AcademicPortal.MVVM.Models.Storage;
using Maui.AcademicPortal.MVVM.ViewModels;

namespace Maui.AcademicPortal
{
    public partial class App : Application
    {
        public readonly ViewModelState ViewModelState;
        public readonly LocalDbService DbService;

        public App(LocalDbService localDbService)
        {
            this.DbService = localDbService;
            this.ViewModelState = new ViewModelState(new ModelState());

            InitializeComponent();

            var navigation = new NavigationPage(new AppShell());
            this.MainPage = navigation;
        }
    }
}
