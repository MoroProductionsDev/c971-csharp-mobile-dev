﻿using Maui.AcademicPortal.MVVM.Models;

namespace Maui.AcademicPortal.MVVM.ViewModels
{
    public class ViewModelState
    {
        public PortalViewModel PortalViewModel { get; private set; } = new();
        public ValidatableTermViewModel? ValidatableTermViewModel { get; set; }
        public ValidatableCourseViewModel? ValidatableCourseViewModel { get; set; }
        public SelectableTermViewModel? SelectableTermViewModel { get; set; }
        public SelectableCourseViewModel? SelectableCourseViewModel { get; set; }

        public ViewModelState()
        {
        }

        public ViewModelState(ModelState modelState)
        {
            this.PortalViewModel = new(modelState.PortalModel);
        }

        public TermViewModel NewTermViewModel()
        {
            return new TermViewModel(new Term());
        }

        public TermViewModel NewTermViewModel(object? selectedTerm)
        {
            if (selectedTerm is null)
            {
                throw new ArgumentNullException(nameof(selectedTerm));
            }
            else if (!selectedTerm.GetType().Equals(typeof(Term)))
            {
                throw new InvalidCastException(nameof(selectedTerm));
            }

            return new TermViewModel((Term) selectedTerm);
        }

        public void NewValidatableTermViewModel()
        {
            this.ValidatableTermViewModel = new ValidatableTermViewModel();
        }

        public void NewSelectableTermViewModel(IEnumerable<object> allTerms, object selectedTerm)
        {
            if (allTerms is null || selectedTerm is null)
            {
                throw new ArgumentNullException();
            }
            else if (!allTerms.GetType().Equals(typeof(IEnumerable<Term>)) &&
                     !selectedTerm.GetType().Equals(typeof(Term)))
            {
                throw new InvalidCastException();
            }

            this.SelectableTermViewModel = 
                new SelectableTermViewModel((IEnumerable<Term>) allTerms, (Term)selectedTerm);
        }

        public CourseViewModel NewCourseViewModel()
        {
            return new CourseViewModel(new Course());
        }

        public CourseViewModel NewCourseViewModel(object? selectedCourse)
        {
            if (selectedCourse is null)
            {
                throw new ArgumentNullException(nameof(selectedCourse));
            }
            else if (!selectedCourse.GetType().Equals(typeof(Course)))
            {
                throw new InvalidCastException(nameof(selectedCourse));
            }

            return new CourseViewModel((Course) selectedCourse);
        }

        public void NewValidatableCourseViewModel()
        {
            this.ValidatableCourseViewModel = new ValidatableCourseViewModel();
        }

        public void NewSelectableCourseViewModel(IEnumerable<object> allCourses, object selectedCourse)
        {
            if (allCourses is null || selectedCourse is null)
            {
                throw new ArgumentNullException();
            }
            else if (!allCourses.GetType().Equals(typeof(IEnumerable<Course>)) &&
                     !selectedCourse.GetType().Equals(typeof(Course)))
            {
                throw new InvalidCastException();
            }

            this.SelectableCourseViewModel =
                new SelectableCourseViewModel((IEnumerable<Course>)allCourses, (Course)selectedCourse);
        }
    }
}