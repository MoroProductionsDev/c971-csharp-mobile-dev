﻿using System.Windows.Input;
using Maui.AcademicPortal.MVVM.ViewModels.Validation;

namespace Maui.AcademicPortal.MVVM.ViewModels
{
    public class ValidatableTermViewModel
    {
        public ValidatableTermViewModel()
        {
            AddValidations();
        }

        // ** Validation
        // Reference: https://learn.microsoft.com/en-us/dotnet/architecture/maui/validation
        public ValidatableObject<string> vTitle { get; private set; } = new(string.Empty);

        public ValidatableObject<DateTime> vStartDate { get; set; } = new(DateTime.Now.Date, DateTime.Now.Date);

        public ValidatableObject<DateTime> vEndDate { get; set; } = new(DateTime.Now.Date, DateTime.Now.Date);

        public ICommand ValidateTitleCommand => new Command(new Action(() => ValidateTitle()));
        public ICommand ValidateStartDateCommand => new Command(new Action(() => ValidateStartDate()));
        public ICommand ValidateEndDateCommand => new Command(new Action(() => ValidateEndDate()));

        public bool HasDataChanged {  get; private set; }

        // ** Copy it to the CourseViewModal so it can trigger the Bindings (Don't use [CurrentTerm])
        public void CopyDataTo(TermViewModel termViewModel)
        {
            termViewModel.Title = this.vTitle.Value;
            termViewModel.StartDate = this.vStartDate.Value;
            termViewModel.EndDate = this.vEndDate.Value;
        }

        // ** Copy it directly from the [CurrentTerm] property.
        public void CopyDataFrom(TermViewModel termViewModel)
        {
            this.vTitle.Value = termViewModel.CurrentTerm.Title;
            this.vStartDate.Value = termViewModel.CurrentTerm.StartDate;
            this.vEndDate.Value = termViewModel.CurrentTerm.EndDate;
        }

        // ** Check it directly from the [CurrentTerm] property.
        public bool CheckIfDataChange(TermViewModel termViewModel)
        {
            HasDataChanged = termViewModel.CurrentTerm.Title != this.vTitle.Value ||
            termViewModel.CurrentTerm.StartDate.ToShortDateString() != this.vStartDate.Value.ToShortDateString() ||
            termViewModel.CurrentTerm.EndDate.ToShortDateString() != this.vEndDate.Value.ToShortDateString();

            return HasDataChanged;
        }

        public bool Validate()
        {
            bool isValidTitle = ValidateTitle();
            bool isValidStart = ValidateStartDate();
            bool isValidEndDate = ValidateEndDate();

            return isValidTitle && isValidStart && isValidEndDate;
        }

        private bool ValidateTitle()
        {
            return this.vTitle.Validate();
        }

        private bool ValidateStartDate()
        {
            return this.vStartDate.ValidateComparison(this.vEndDate.Value);
        }

        private bool ValidateEndDate()
        {
            return this.vEndDate.ValidateComparison(this.vStartDate.Value);
        }

        private void AddValidations()
        {
            vTitle.Validations.Add(new NullOrEmptyRule<string>
            {
                ValidationMessage = "A title is required."
            });

            //vStartDate.Validations.Add(new IsNotPastDate<DateTime>
            //{
            //    ValidationMessage = $"Start date can not be previous today's date ."
            //});

            vStartDate.ComparisonValidations.Add(new PostDateRule<DateTime>
            {
                ValidationMessage = $"Start date can not be post the end date."
            });

            //vEndDate.Validations.Add(new IsNotPastDate<DateTime>
            //{
            //    ValidationMessage = $"End date can not be previous today's date ."
            //});

            vEndDate.ComparisonValidations.Add(new PastDateRule<DateTime>
            {
                ValidationMessage = $"End date can not be previous the start date."
            });
        }
    }
}
