﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using CommunityToolkit.Maui.Core.Extensions;
using Maui.AcademicPortal.MVVM.Models;

namespace Maui.AcademicPortal.MVVM.ViewModels
{
    public class TermViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        public const int MAX_COURSES_PER_TERM = 6;
        public Term CurrentTerm { get; private set; }

        public TermViewModel(Term term)
        {
            this.CurrentTerm = term;
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Title
        {
            get =>  this.CurrentTerm.Title;
            set
            {
                this.CurrentTerm.Title = value;
                
                NotifyPropertyChanged(nameof(Title));
            }
        }

        // Reference: https://medium.com/@niteshsinghal85/converting-dateonly-and-timeonly-to-datetime-and-vice-versa-in-net-6-72db56853857
        public DateTime StartDate
        {
            get => this.CurrentTerm.StartDate;
            set
            {
                this.CurrentTerm.StartDate = value;
                NotifyPropertyChanged(nameof(StartDate));
            }
        }
        public DateTime EndDate
        {
            get => DateTime.Now;
            set
            {
                this.CurrentTerm.EndDate = value;
                NotifyPropertyChanged(nameof(EndDate));
            }
        }

        public ObservableCollection<Course> Courses
        {
            get => this.CurrentTerm.Courses.ToObservableCollection<Course>();
        }

        public async Task FetchCourses()
        {
            await CurrentTerm.GetCourses();
        }
    }
}
