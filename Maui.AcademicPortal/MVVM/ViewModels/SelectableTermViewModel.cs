﻿using Maui.AcademicPortal.MVVM.Models;

namespace Maui.AcademicPortal.MVVM.ViewModels
{
    public class SelectableTermViewModel : TermViewModel
    {
        public int SelectedIdx = INDEX_NOT_FOUND;
        private readonly IEnumerable<Term> terms;
        private Term? _selectedTerm;
        const int INDEX_NOT_FOUND = -1;
        public SelectableTermViewModel(IEnumerable<Term> terms, Term selectedTerm) : base(selectedTerm)
        {
            this.terms = terms;
            this._selectedTerm = selectedTerm;
            this.SelectedIdx = SelectedTermIndex(this._selectedTerm);
        }

        public object? SelectedTerm
        {
            get => this._selectedTerm;
            set
            {
                if (value != null && value.GetType().Equals(typeof(Term)))
                {
                    if (value != this._selectedTerm)
                    {
                        this._selectedTerm = (Term)value;
                        this.SelectedIdx = SelectedTermIndex(this._selectedTerm);
                    }
                }
            }
        }

        public async Task AddCourse(Course newCourse)
        {
            await this.CurrentTerm.AddCourse(newCourse);
            NotifyPropertyChanged(nameof(Courses));
        }

        public async Task EditCourse(Course editedCourse, int idx)
        {
            await this.CurrentTerm.EditCourse(editedCourse, idx);
            NotifyPropertyChanged(nameof(Courses));
        }

        public async Task RemoveCourse(Course deleteCourse, int idx)
        {
            await this.CurrentTerm.RemoveCourse(deleteCourse, idx);
            NotifyPropertyChanged(nameof(Courses));
        }

        public List<Course>? GetSelectedItemCourses()
        {
            if (null == _selectedTerm)
            {
                return null;
            }

            return this._selectedTerm.Courses;
        }

        public int GetSelectedItemCoursesCount()
        {
            if (null == _selectedTerm)
            {
                return INDEX_NOT_FOUND;
            }

            return this._selectedTerm.Courses.Count;
        }

        // --------------------------------------------------------
        private int SelectedTermIndex(Term newSelectedTerm)
        {
            int index = 0;

            foreach (var term in this.terms)
            {
                if (term == newSelectedTerm)
                {
                    return index;
                }
                index++;
            }
            return INDEX_NOT_FOUND;
        }
    }
}
