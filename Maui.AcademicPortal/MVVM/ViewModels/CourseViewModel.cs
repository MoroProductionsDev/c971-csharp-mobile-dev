﻿using System.ComponentModel;
using Maui.AcademicPortal.MVVM.Models;

namespace Maui.AcademicPortal.MVVM.ViewModels
{
    public class CourseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        public Course CurrentCourse { get; private set; }
        public bool HasObjectiveAssessment { get; set; }
        public bool HasPerformanceAssessment { get; set; }
        public CourseViewModel(Course courseModel)
        {
            this.CurrentCourse = courseModel;

            HasObjectiveAssessment = this.CurrentCourse.ObjectiveAssessment is not null ? true : false;
            HasPerformanceAssessment = this.CurrentCourse.PerformanceAssessment is not null ? true : false;
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public String Name
        {
            get => this.CurrentCourse.Name;
            set
            {
                this.CurrentCourse.Name = value;
                NotifyPropertyChanged(nameof(Name));
            }
        }
        public DateTime StartDate
        {
            get => this.CurrentCourse.StartDate;
            set
            {
                this.CurrentCourse.StartDate = value;
                NotifyPropertyChanged(nameof(StartDate));
            }
        }
        public DateTime EndDate
        {
            get => this.CurrentCourse.EndDate;
            set
            {
                this.CurrentCourse.EndDate = value;
                NotifyPropertyChanged(nameof(EndDate));
            }
        }

        public CourseStatusEnum Status
        {
            get => CurrentCourse.Status;
            set
            {
                this.CurrentCourse.Status = value;

                NotifyPropertyChanged(nameof(Status));
            }
        }

        public String Note
        {
            get => this.CurrentCourse.Note;
            set
            {
                this.CurrentCourse.Note = value;
                NotifyPropertyChanged(nameof(Note));
            }
        }

        // --------------------------------------------------------
        public string InstructorName
        {
            get => this.CurrentCourse.Instructor.Name;
            set
            {
                this.CurrentCourse.Instructor.Name = value;
                NotifyPropertyChanged(nameof(InstructorName));
            }
        }

        public string InstructorPhone
        {
            get => this.CurrentCourse.Instructor.Phone;
            set
            {
                this.CurrentCourse.Instructor.Phone = value;
                NotifyPropertyChanged(nameof(InstructorPhone));
            }
        }

        public string InstructorEmail
        {
            get => this.CurrentCourse.Instructor.Email;
            set
            {
                this.CurrentCourse.Instructor.Email = value;
                NotifyPropertyChanged(nameof(InstructorEmail));
            }
        }

        // --------------------------------------------------------
        // Reference: https://medium.com/@niteshsinghal85/converting-dateonly-and-timeonly-to-datetime-and-vice-versa-in-net-6-72db56853857
        public string? ObjectiveAssessmentName
        {
            get => this.CurrentCourse.ObjectiveAssessment?.Name;
            set
            {
                if (this.CurrentCourse.ObjectiveAssessment is not null &&
                    value is not null)
                {
                    this.CurrentCourse.ObjectiveAssessment.Name = (string)value;
                    NotifyPropertyChanged(nameof(ObjectiveAssessmentName));
                }
            }
        }

        public DateTime? ObjectiveAssessmentStartDate
        {
            get => this.CurrentCourse.ObjectiveAssessment?.StartDate;
            set
            {
                if (this.CurrentCourse.ObjectiveAssessment is not null &&
                    value is not null)
                {
                    this.CurrentCourse.ObjectiveAssessment.StartDate = (DateTime) value;
                    NotifyPropertyChanged(nameof(ObjectiveAssessmentStartDate));
                }
            }
        }
        public DateTime? ObjectiveAssessmentEndDate
        {
            get => this.CurrentCourse.ObjectiveAssessment?.EndDate;
            set
            {
                if (this.CurrentCourse.ObjectiveAssessment is not null &&
                    value is not null)
                {
                    this.CurrentCourse.ObjectiveAssessment.EndDate = (DateTime) value;
                    NotifyPropertyChanged(nameof(ObjectiveAssessmentEndDate));
                }
            }
        }

        // --------------------------------------------------------
        public string? PerformanceAssessmentName
        {
            get => this.CurrentCourse.PerformanceAssessment?.Name;
            set
            {
                if (this.CurrentCourse.PerformanceAssessment is not null &&
                value is not null)
                {
                    this.CurrentCourse.PerformanceAssessment.Name = value;
                    NotifyPropertyChanged(nameof(PerformanceAssessmentName));
                }
            }
        }

        public DateTime? PerformanceAssessmentStartDate
        {
            get => this.CurrentCourse.PerformanceAssessment?.StartDate;
            set
            {
                if (this.CurrentCourse.PerformanceAssessment is not null &&
                    value is not null)
                {
                    this.CurrentCourse.PerformanceAssessment.StartDate = (DateTime)value;
                    NotifyPropertyChanged(nameof(PerformanceAssessmentStartDate));
                }
            }
        }
        public DateTime? PerformanceAssessmentEndDate
        {
            get => this.CurrentCourse.PerformanceAssessment?.EndDate;
            set
            {
                if (this.CurrentCourse.PerformanceAssessment is not null &&
                value is not null)
                {
                    this.CurrentCourse.PerformanceAssessment.EndDate = (DateTime)value;
                    NotifyPropertyChanged(nameof(PerformanceAssessmentEndDate));
                }
            }
        }
    }   
}
