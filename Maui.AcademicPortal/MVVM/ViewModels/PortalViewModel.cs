﻿using System.ComponentModel;
using System.Collections.ObjectModel;
using CommunityToolkit.Maui.Core.Extensions;
using Maui.AcademicPortal.MVVM.Models;

namespace Maui.AcademicPortal.MVVM.ViewModels
{
    public class PortalViewModel
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        private Portal currentPortal;


        public PortalViewModel()
        {
            currentPortal = new();
        }

        public PortalViewModel(Portal portalModel)
        {
            this.currentPortal = portalModel;
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ObservableCollection<Term> Terms
        {
            get => this.currentPortal.Terms.ToObservableCollection();
        }

        public async Task FetchTerms()
        {
            await currentPortal.GetTerms();
        }

        public async Task AddTerm(Term newTerm)
        {
            await this.currentPortal.AddTerm(newTerm);
            NotifyPropertyChanged(nameof(Terms));
        }

        public async Task EditTerm(Term editedTerm, int idx)
        {
            await this.currentPortal.EditTerm(editedTerm, idx);
            NotifyPropertyChanged(nameof(Terms));
        }

        public async Task RemoveTerm(Term deleteTerm, int idx)
        {
            await this.currentPortal.RemoveTerm(deleteTerm, idx);
            NotifyPropertyChanged(nameof(Terms));
        }
    }
}
