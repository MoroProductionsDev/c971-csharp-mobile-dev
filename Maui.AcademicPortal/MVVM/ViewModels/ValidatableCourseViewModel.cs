﻿using System.Windows.Input;
using Maui.AcademicPortal.MVVM.Models;
using Maui.AcademicPortal.MVVM.ViewModels.Validation;
using Maui.AcademicPortal.MVVM.ViewModels.Validation.Conditions;

namespace Maui.AcademicPortal.MVVM.ViewModels
{
    public class ValidatableCourseViewModel
    {
        public bool HasObjectiveAssessment { get; private set; }
        public bool HasPerformanceAssessment { get; private set; }

        private List<CourseStatus> _courseStatus = new List<CourseStatus>()
        {
            new CourseStatus(CourseStatusEnum.Scheduled),
            new CourseStatus(CourseStatusEnum.Progress),
            new CourseStatus(CourseStatusEnum.Completed),
            new CourseStatus(CourseStatusEnum.Dropped),
        };

        public ValidatableCourseViewModel()
        {
            AddValidations();
        }

        public IList<CourseStatus> CourseStatuses
        {
            get => _courseStatus;
        }

        // ** Validation
        public ValidatableObject<string> vName { get; set; } = new(String.Empty);

        public ValidatableObject<DateTime> vStartDate { get; set; } = new(DateTime.Now.Date, DateTime.Now.Date);

        public ValidatableObject<DateTime> vEndDate { get; set; } = new(DateTime.Now.Date, DateTime.Now.Date);

        public ValidatableObject<CourseStatusEnum> vStatus { get; set; } = new(CourseStatusEnum.Scheduled);

        public ValidatableObject<int> vSelectedStatusIdx { get; set; } = new(SelectedIndexRule<int>.IDX_NOT_FOUND);
        
        public ValidatableObject<string> vNote { get; set; } = new(String.Empty);

        public ValidatableObject<string> vInstructorName { get; set; } = new(String.Empty);

        public ValidatableObject<string> vInstructorPhone { get; set; } = new(String.Empty);

        public ValidatableObject<string> vInstructorEmail { get; set; } = new(String.Empty);

        public ValidatableObject<string> vObjectiveAssessmentName { get; set; } = new(String.Empty);

        public ValidatableObject<DateTime> vObjectiveAssessmentStartDate { get; set; } = new(DateTime.Now.Date, DateTime.Now.Date);

        public ValidatableObject<DateTime> vObjectiveAssessmentEndDate { get; set; } = new(DateTime.Now.Date, DateTime.Now.Date);

        public ValidatableObject<string> vPerformanceAssessmentName { get; set; } = new(String.Empty);

        public ValidatableObject<DateTime> vPerformanceAssessmentStartDate { get; set; } = new(DateTime.Now.Date, DateTime.Now.Date);

        public ValidatableObject<DateTime> vPerformanceAssessmentEndDate { get; set; } = new(DateTime.Now.Date, DateTime.Now.Date);

        public ICommand ValidateNameCommand => new Command(new Action(() => ValidateName()));
        public ICommand ValidateStartDateCommand => new Command(new Action(() => ValidateStartDate()));
        public ICommand ValidateEndDateCommand => new Command(new Action(() => ValidateEndDate()));

        public ICommand ValidateInstructorNameCommand => new Command(new Action(() => ValidateInstructorName()));
        public ICommand ValidateInstructorPhoneCommand => new Command(new Action(() => ValidateInstructorPhone()));
        public ICommand ValidateInstructorEmailCommand => new Command(new Action(() => ValidateInstructorEmail()));

        public ICommand ValidateObjectiveAssessmentNameCommand => new Command(new Action(() => {
            if (HasObjectiveAssessment)
            {
                ValidateObjectiveAssessmentName();
            }
        }));
        public ICommand ValidateObjectiveAssessmentStartDateCommand => new Command(new Action(() =>
        {
            if (HasObjectiveAssessment)
            {
                ValidateObjectiveAssessmentStartDate();
            }
        }));
        public ICommand ValidateObjectiveAssessmentEndDateCommand => new Command(new Action(() =>
        {
            if (HasObjectiveAssessment)
            {
                ValidateObjectiveAssessmentEndDate();
            }
        }));

        // -------------------------------------------------------------------------------------

        public ICommand ValidatePerformanceAssessmentNameCommand => new Command(new Action(() =>
        {
            if (HasPerformanceAssessment)
            {
                ValidatePerformanceAssessmentName();
            }
        }));

        public ICommand ValidatePerformanceAssessmentStartDateCommand => new Command(new Action(() =>
        {
            if (HasPerformanceAssessment)
            {
                ValidatePerformanceAssessmentStartDate();
            }
        }));
        public ICommand ValidatePerformanceAssessmentEndDateCommand => new Command(new Action(() =>
        {
            if (HasPerformanceAssessment)
            {
                ValidatePerformanceAssessmentEndDate();
            }
        }));

        public bool HasDataChanged { get; private set; }

        // ** Copy it to the CourseViewModal so it can trigger the Bindings. (Don't use [CurrentCourse])
        public void CopyDataTo(CourseViewModel courseViewModel)
        {
            courseViewModel.Name = this.vName.Value;
            courseViewModel.StartDate = this.vStartDate.Value;
            courseViewModel.EndDate = this.vEndDate.Value;
            courseViewModel.Status = this.vStatus.Value;
            courseViewModel.Note = this.vNote.Value;
            courseViewModel.InstructorName = this.vInstructorName.Value;
            courseViewModel.InstructorPhone = this.vInstructorPhone.Value;
            courseViewModel.InstructorEmail = this.vInstructorEmail.Value;

            if (courseViewModel.CurrentCourse.ObjectiveAssessment is not null)
            {
                courseViewModel.ObjectiveAssessmentName = this.vObjectiveAssessmentName.Value;
                courseViewModel.ObjectiveAssessmentStartDate = this.vObjectiveAssessmentStartDate.Value;
                courseViewModel.ObjectiveAssessmentEndDate = this.vObjectiveAssessmentEndDate.Value;
            }


            if (courseViewModel.CurrentCourse.PerformanceAssessment is not null)
            {
                courseViewModel.PerformanceAssessmentName = this.vPerformanceAssessmentName.Value;
                courseViewModel.PerformanceAssessmentStartDate = this.vPerformanceAssessmentStartDate.Value;
                courseViewModel.PerformanceAssessmentEndDate = this.vPerformanceAssessmentEndDate.Value;
            }
        }

        // ** Copy it directly from the [CurrentCourse] property.
        public void CopyDataFrom(CourseViewModel courseViewModel)
        {
            this.vName.Value = courseViewModel.CurrentCourse.Name;
            this.vStartDate.Value = courseViewModel.CurrentCourse.StartDate;
            this.vEndDate.Value = courseViewModel.CurrentCourse.EndDate;
            this.vStatus.Value = courseViewModel.CurrentCourse.Status;
            this.vNote.Value = courseViewModel.CurrentCourse.Note;
            this.vInstructorName.Value = courseViewModel.CurrentCourse.Instructor.Name;
            this.vInstructorPhone.Value = courseViewModel.CurrentCourse.Instructor.Phone;
            this.vInstructorEmail.Value = courseViewModel.InstructorEmail;

            if(courseViewModel.CurrentCourse.ObjectiveAssessment is not null)
            {
                this.vObjectiveAssessmentName.Value = courseViewModel.CurrentCourse.ObjectiveAssessment.Name;
                this.vObjectiveAssessmentStartDate.Value = courseViewModel.CurrentCourse.ObjectiveAssessment.StartDate;
                this.vObjectiveAssessmentEndDate.Value = courseViewModel.CurrentCourse.ObjectiveAssessment.EndDate;
                
                HasObjectiveAssessment = true;
            }
            else
            {
                HasObjectiveAssessment = false;
            }

            if (courseViewModel.CurrentCourse.PerformanceAssessment is not null)
            {
                this.vPerformanceAssessmentName.Value = courseViewModel.CurrentCourse.PerformanceAssessment.Name;
                this.vPerformanceAssessmentStartDate.Value = courseViewModel.CurrentCourse.PerformanceAssessment.StartDate; ;
                this.vPerformanceAssessmentEndDate.Value = courseViewModel.CurrentCourse.PerformanceAssessment.EndDate;

                HasPerformanceAssessment = true;
            }
            else
            {
                HasPerformanceAssessment = false;
            }
        }

        // ** Check it directly from the [CurrentTCourse] property.
        public bool CheckIfDataChange(CourseViewModel courseViewModel)
        {
            HasDataChanged = courseViewModel.Name != this.vName.Value ||
                courseViewModel.CurrentCourse.StartDate.ToShortDateString() != this.vStartDate.Value.ToShortDateString() ||
                courseViewModel.CurrentCourse.EndDate.ToShortDateString() != this.vEndDate.Value.ToShortDateString() ||
                courseViewModel.CurrentCourse.Status != this.vStatus.Value ||
                courseViewModel.CurrentCourse.Note != this.vNote.Value ||
                courseViewModel.CurrentCourse.Instructor.Name != this.vInstructorName.Value ||
                courseViewModel.CurrentCourse.Instructor.Phone != this.vInstructorPhone.Value ||
                courseViewModel.CurrentCourse.Instructor.Email != this.vInstructorEmail.Value;



            if (courseViewModel.CurrentCourse.ObjectiveAssessment is not null &&
                !HasDataChanged)
            {
                HasDataChanged = courseViewModel.CurrentCourse.ObjectiveAssessment.Name != this.vObjectiveAssessmentName.Value ||
                    courseViewModel.CurrentCourse.ObjectiveAssessment.StartDate.ToShortDateString() != this.vObjectiveAssessmentStartDate.Value.ToShortDateString() ||
                    courseViewModel.CurrentCourse.ObjectiveAssessment.EndDate.ToShortDateString() != this.vObjectiveAssessmentEndDate.Value.ToShortDateString();
                    
            }

            if (courseViewModel.CurrentCourse.PerformanceAssessment is not null &&
                !HasDataChanged)
            {
                    HasDataChanged = courseViewModel.CurrentCourse.PerformanceAssessment.Name != this.vPerformanceAssessmentName.Value ||
                    courseViewModel.CurrentCourse.PerformanceAssessment.StartDate!.ToShortDateString() != this.vPerformanceAssessmentStartDate.Value.ToShortDateString() ||
                    courseViewModel.CurrentCourse.PerformanceAssessment.EndDate.ToShortDateString() != this.vPerformanceAssessmentEndDate.Value.ToShortDateString();
            }

            return HasDataChanged;
        }

        public bool Validate()
        {
            bool isValidTitle = ValidateName();
            bool isValidStartDate = ValidateStartDate();
            bool isValidEndDate = ValidateEndDate();
            bool isValidInstructorName = ValidateInstructorName();
            bool isValidInstructorPhone = ValidateInstructorPhone();
            bool isValidInstructorEmail = ValidateInstructorEmail();

            bool isValidObjectiveAssessmentName = ValidateObjectiveAssessmentName();
            bool isValidObjectiveAssessmentStartDate = ValidateObjectiveAssessmentStartDate();
            bool isValidObjectiveAssessmentEndDate = ValidateObjectiveAssessmentEndDate();

            bool isValidPerformanceAssessmentName = ValidatePerformanceAssessmentName();
            bool isValidPerformanceAssessmentStartDate = ValidatePerformanceAssessmentStartDate();
            bool isValidPerformanceAssessmentEndDate = ValidatePerformanceAssessmentEndDate();

            bool isValidSelectedStatusIdx = ValidateSelectedStatusIdx();


            if (HasObjectiveAssessment && !HasPerformanceAssessment)
            {
                return isValidTitle && isValidStartDate && isValidEndDate && isValidSelectedStatusIdx &&
                    isValidInstructorName && isValidInstructorPhone && isValidInstructorEmail &&
                    isValidObjectiveAssessmentName && isValidObjectiveAssessmentStartDate && isValidObjectiveAssessmentEndDate;
            }
            else  if (!HasObjectiveAssessment && HasPerformanceAssessment)
            {
                return isValidTitle && isValidStartDate && isValidEndDate && isValidSelectedStatusIdx &&
                    isValidInstructorName && isValidInstructorPhone && isValidInstructorEmail &&
                    isValidPerformanceAssessmentName && isValidPerformanceAssessmentStartDate && isValidPerformanceAssessmentEndDate;
            } else
            {
                return isValidTitle && isValidStartDate && isValidEndDate && isValidSelectedStatusIdx &&
                    isValidInstructorName && isValidInstructorPhone && isValidInstructorEmail && 
                    isValidObjectiveAssessmentName && isValidObjectiveAssessmentStartDate && isValidObjectiveAssessmentEndDate && 
                    isValidPerformanceAssessmentName && isValidPerformanceAssessmentStartDate && isValidPerformanceAssessmentEndDate;
            }
        }

        private bool ValidateName()
        {
            return this.vName.Validate();
        }

        private bool ValidateStartDate()
        {
            return this.vStartDate.ValidateComparison(this.vEndDate.Value);
        }

        private bool ValidateEndDate()
        {
            return this.vEndDate.ValidateComparison(this.vStartDate.Value);
        }

        private bool ValidateInstructorName()
        {
            return this.vInstructorName.Validate();
        }

        private bool ValidateInstructorPhone()
        {
            return this.vInstructorPhone.Validate();
        }

        private bool ValidateInstructorEmail()
        {
            return this.vInstructorEmail.Validate();
        }

        private bool ValidateObjectiveAssessmentName()
        {
            return this.vObjectiveAssessmentName.Validate();
        }

        private bool ValidateObjectiveAssessmentStartDate()
        {
            return this.vObjectiveAssessmentStartDate.ValidateComparison(this.vObjectiveAssessmentEndDate.Value);
        }

        private bool ValidateObjectiveAssessmentEndDate()
        {
            return this.vObjectiveAssessmentEndDate.ValidateComparison(this.vObjectiveAssessmentStartDate.Value);
        }

        private bool ValidatePerformanceAssessmentName()
        {
            return this.vPerformanceAssessmentName.Validate();
        }

        private bool ValidatePerformanceAssessmentStartDate()
        {
            return this.vPerformanceAssessmentStartDate.ValidateComparison(this.vPerformanceAssessmentEndDate.Value);
        }

        private bool ValidatePerformanceAssessmentEndDate()
        {
            return this.vPerformanceAssessmentEndDate.ValidateComparison(this.vPerformanceAssessmentStartDate.Value);
        }

        private bool ValidateSelectedStatusIdx()
        {
            return this.vSelectedStatusIdx.Validate();
        }


        private void AddValidations()
        {
            vName.Validations.Add(new NullOrEmptyRule<string>
            {
                ValidationMessage = "A name is required."
            });
            //vStartDate.Validations.Add(new IsNotPastDate<DateTime>
            //{
            //    ValidationMessage = $"Start date can not be previous today's date ."
            //});

            vStartDate.ComparisonValidations.Add(new PostDateRule<DateTime>
            {
                ValidationMessage = $"Start date can not be post the end date."
            });

            //vEndDate.Validations.Add(new IsNotPastDate<DateTime>
            //{
            //    ValidationMessage = $"End date can not be previous today's date ."
            //});

            vEndDate.ComparisonValidations.Add(new PastDateRule<DateTime>
            {
                ValidationMessage = $"End date can not be previous the start date."
            });

            vInstructorName.Validations.Add(new NullOrEmptyRule<string>
            {
                ValidationMessage = "A name is required."
            });

            //

            vInstructorPhone.Validations.Add(new NullOrEmptyRule<string>
            {
                ValidationMessage = "A phone is required."
            });

            vInstructorPhone.Validations.Add(new PhoneRule<string>
            {
                ValidationMessage = "Invalid phone format."
            });

            // 

            vInstructorEmail.Validations.Add(new NullOrEmptyRule<string>
            {
                ValidationMessage = "An email  is required."
            });

            vInstructorEmail.Validations.Add(new EmailRule<string>
            {
                ValidationMessage = "Invalid email format."
            });

            //

            vObjectiveAssessmentName.Validations.Add(new NullOrEmptyRule<string>
            {
                ValidationMessage = "A name is required."
            });

            //vObjectiveAssessmentStartDate.Validations.Add(new IsNotPastDate<DateTime>
            //{
            //    ValidationMessage = $"Start date can not be previous today's date ."
            //});

            vObjectiveAssessmentStartDate.ComparisonValidations.Add(new PostDateRule<DateTime>
            {
                ValidationMessage = $"Start date can not be post the end date."
            });

            //vEndDate.Validations.Add(new IsNotPastDate<DateTime>
            //{
            //    ValidationMessage = $"End date can not be previous today's date ."
            //});

            vObjectiveAssessmentEndDate.ComparisonValidations.Add(new PastDateRule<DateTime>
            {
                ValidationMessage = $"End date can not be previous the start date."
            });

            //
            vPerformanceAssessmentName.Validations.Add(new NullOrEmptyRule<string>
            {
                ValidationMessage = "A name is required."
            });

            //vPerformanceAssessmentEndDate.Validations.Add(new IsNotPastDate<DateTime>
            //{
            //    ValidationMessage = $"Start date can not be previous today's date ."
            //});

            vPerformanceAssessmentStartDate.ComparisonValidations.Add(new PostDateRule<DateTime>
            {
                ValidationMessage = $"Start date can not be post the end date."
            });

            //vPerformanceAssessmentEndDate.Validations.Add(new IsNotPastDate<DateTime>
            //{
            //    ValidationMessage = $"End date can not be previous today's date ."
            //});

            vPerformanceAssessmentEndDate.ComparisonValidations.Add(new PastDateRule<DateTime>
            {
                ValidationMessage = $"End date can not be previous the start date."
            });

            vSelectedStatusIdx.Validations.Add(new SelectedIndexRule<int>
            {
                ValidationMessage = $"Select a course status"
            });
        }
    }

    public class CourseStatus
    {
        public CourseStatusEnum Status;
        public string Name { get; private set; } = string.Empty;
        public int Idx { get; private set; } = -1;

        const int SCHEDULED_IDX = 0;
        const int PROGRESS_IDX = 1;
        const int COMPLETED_IDX = 2;
        const int DROPPED_IDX = 3;

        public CourseStatus(CourseStatusEnum courseStatus)
        {
            switch (courseStatus)
            {
                case CourseStatusEnum when courseStatus.Equals(CourseStatusEnum.Scheduled):
                    this.Name = CourseStateStr.Scheduled;
                    this.Status = courseStatus;
                    this.Idx = SCHEDULED_IDX;
                    break;
                case CourseStatusEnum when courseStatus.Equals(CourseStatusEnum.Progress):
                    this.Name = CourseStateStr.Progress;
                    this.Status = courseStatus;
                    this.Idx = PROGRESS_IDX;
                    break;
                case CourseStatusEnum when courseStatus.Equals(CourseStatusEnum.Completed):
                    this.Name = CourseStateStr.Completed;
                    this.Status = courseStatus;
                    this.Idx = COMPLETED_IDX;
                    break;
                case CourseStatusEnum when courseStatus.Equals(CourseStatusEnum.Dropped):
                    this.Name = CourseStateStr.Dropped;
                    this.Status = courseStatus;
                    this.Idx = DROPPED_IDX;
                    break;
                default:
                    break;
            }
        }
    }
}


