﻿using Maui.AcademicPortal.MVVM.Models;

namespace Maui.AcademicPortal.MVVM.ViewModels
{
    public class SelectableCourseViewModel : CourseViewModel
    {
        public int SelectedIdx = INDEX_NOT_FOUND;
        private Course? _selectedCourse;
        private readonly IEnumerable<Course> courses;
        const int INDEX_NOT_FOUND = -1;
        public SelectableCourseViewModel(IEnumerable<Course> courses, Course selectedCourse) : base(selectedCourse)
        {
            this.courses = courses;
            this._selectedCourse = selectedCourse;
            this.SelectedIdx = SelectedCourseIndex(this._selectedCourse);
        }

        public object? SelectedCourse
        {
            get => this._selectedCourse;
            set
            {
                if (value != null && value.GetType().Equals(typeof(Course)))
                {
                    this._selectedCourse = (Course)value;
                    this.SelectedIdx = SelectedCourseIndex(this._selectedCourse);
                }
            }
        }

        // --------------------------------------------------------
        private int SelectedCourseIndex(Course newSelectedCourse)
        {
            int index = 0;

            foreach (var term in this.courses)
            {
                if (term == newSelectedCourse)
                {
                    return index;
                }
                index++;
            }
            return INDEX_NOT_FOUND;
        }
    }
}
