﻿using CommunityToolkit.Mvvm.ComponentModel;

namespace Maui.AcademicPortal.MVVM.ViewModels.Validation
{
    // Reference: https://learn.microsoft.com/en-us/dotnet/architecture/maui/validation
    public class ValidatableObject<T> : ObservableObject, IValidity
    {
        private IEnumerable<string> _errors;
        private IEnumerable<string> _comparableErrors;
        private bool _isValid;
        private T _value;
        private T? _secondValue;
        public List<IValidationRule<T>> Validations { get; } = new();

        public List<IComparisonValidationRule<T>> ComparisonValidations { get; } = new();
        public IEnumerable<string> Errors
        {
            get => _errors;
            private set => SetProperty(ref _errors, value);
        }

        public IEnumerable<string> ComparableErrors
        {
            get => _comparableErrors;
            private set => SetProperty(ref _comparableErrors, value);
        }
        public bool IsValid
        {
            get => _isValid;
            private set => SetProperty(ref _isValid, value);
        }
        public T Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }

        public T? ComparedValue
        {
            get => _secondValue;
            private set => _secondValue = value;
        }

        public ValidatableObject(T value)
        {
            _isValid = true;
            _errors = Enumerable.Empty<string>();
            _comparableErrors = Enumerable.Empty<string>();
            _value = value;
        }

        public ValidatableObject(T firstValue, T secondValue)
        {
            _isValid = true;
            _errors = Enumerable.Empty<string>();
            _comparableErrors = Enumerable.Empty<string>();
            _value = firstValue;
            _secondValue = secondValue;
        }

        public bool Validate()
        {
            Errors = Validations
                ?.Where(v => !v.Check(Value))
                ?.Select(v => v.ValidationMessage)
                ?.ToArray()
                ?? Enumerable.Empty<string>();
            IsValid = isValid();
            return IsValid;
        }

        public bool ValidateComparison(T secondValue)
        {
            ComparedValue = secondValue;

            ComparableErrors = ComparisonValidations
                ?.Where(v => !v.Check(Value, ComparedValue))
                ?.Select(v => v.ValidationMessage)
                ?.ToArray()
                ?? Enumerable.Empty<string>();
            IsValid = isValid();
            return IsValid;
        }

        private bool isValid()
        {
            return !Errors.Any() && !ComparableErrors.Any();
        }
    }
}
