﻿namespace Maui.AcademicPortal.MVVM.ViewModels.Validation
{
    interface IValidity
    {
        public bool Validate();
    }
}
