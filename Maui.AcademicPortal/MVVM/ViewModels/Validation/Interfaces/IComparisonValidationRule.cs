﻿namespace Maui.AcademicPortal.MVVM.ViewModels.Validation
{
    public interface IComparisonValidationRule<T>
    {
        string ValidationMessage { get; set; }
        bool Check(T firstValue, T? secondValue);
    }
}
