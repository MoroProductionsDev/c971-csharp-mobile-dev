﻿namespace Maui.AcademicPortal.MVVM.ViewModels.Validation
{
    public interface IValidationRule<T>
    {
        string ValidationMessage { get; set; }
        bool Check(T value);
    }
}
