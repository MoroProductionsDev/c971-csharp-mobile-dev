﻿namespace Maui.AcademicPortal.MVVM.ViewModels.Validation.Conditions
{
    internal class SelectedIndexRule<T> : IValidationRule<T>
    {
        public const int IDX_NOT_FOUND = -1;
        public string ValidationMessage { get; set; } = String.Empty;

        public bool Check(T value) =>
            value is int idx && idx != IDX_NOT_FOUND;
    }
}
