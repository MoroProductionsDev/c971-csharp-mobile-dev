﻿using System.Text.RegularExpressions;

namespace Maui.AcademicPortal.MVVM.ViewModels.Validation.Conditions
{
    // Reference: https://learn.microsoft.com/en-us/dotnet/architecture/maui/validation
    public class EmailRule<T> : IValidationRule<T>
    {
        // Reference: https://www.abstractapi.com/guides/email-validation/validate-emails-in-c
        private readonly Regex _regex = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

        public string ValidationMessage { get; set; } = String.Empty;

        public bool Check(T value) =>
            value is string str && _regex.IsMatch(str);
    }
}
