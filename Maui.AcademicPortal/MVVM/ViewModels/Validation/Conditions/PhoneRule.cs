﻿using System.Text.RegularExpressions;

namespace Maui.AcademicPortal.MVVM.ViewModels.Validation.Conditions
{
    public class PhoneRule<T> : IValidationRule<T>
    {
        // Reference: https://regex101.com/r/ll3MpB/1
        private readonly Regex _regex = new Regex("\\+?[\\d]?\\s*\\(?\\d{3}\\)?[\\s\\.-]?\\d{3}[\\s\\.-]?\\d{4}");

        public string ValidationMessage { get; set; } = String.Empty;

        public bool Check(T value) =>
            value is string str && _regex.IsMatch(str);
    }
}
