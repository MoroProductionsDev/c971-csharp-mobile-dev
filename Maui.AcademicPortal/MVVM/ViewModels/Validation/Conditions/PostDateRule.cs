﻿namespace Maui.AcademicPortal.MVVM.ViewModels.Validation
{
    public class PostDateRule<T> : IComparisonValidationRule<T>
    {
        public string ValidationMessage { get; set; } = String.Empty;

        public bool Check(T firstValue, T? secondVal)
        {
            if (firstValue is DateTime firstDate &&
                secondVal is DateTime secondDate &&
                DateTime.Compare(firstDate.Date, secondDate.Date) <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
