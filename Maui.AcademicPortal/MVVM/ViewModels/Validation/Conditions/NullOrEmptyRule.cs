﻿namespace Maui.AcademicPortal.MVVM.ViewModels.Validation
{
    public class NullOrEmptyRule<T> : IValidationRule<T> 
    {
        public string ValidationMessage { get; set; } = String.Empty;

        public bool Check(T value) =>
            value is string str && !string.IsNullOrWhiteSpace(str);
    }
}
