﻿namespace Maui.AcademicPortal.MVVM.ViewModels.Validation
{
    public class PastDateRule<T> : IValidationRule<T>, IComparisonValidationRule<T>
    {
        public string ValidationMessage { get; set; } = String.Empty;

        public bool Check(T value) {
            if (value is DateTime date && DateTime.Compare(date.Date, DateTime.Now.Date) >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Check(T firstValue, T? secondVal)
        {
            if (firstValue is DateTime firstDate &&
                secondVal is DateTime secondDate &&
                DateTime.Compare(firstDate.Date, secondDate.Date) >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
