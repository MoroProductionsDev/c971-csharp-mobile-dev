﻿namespace Maui.AcademicPortal.MVVM.ViewModels.Validation
{
    internal static class ValueValidator
    {
        internal static bool CheckingForTodaysDate(DateTime currentDateTime)
        {
            return currentDateTime.Date.Equals(DateTime.Now.Date);
        }
    }
}
