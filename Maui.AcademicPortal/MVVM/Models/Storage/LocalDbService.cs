﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Maui.AcademicPortal.MVVM.Models.Storage
{
    // Reference: https://learn.microsoft.com/en-us/dotnet/maui/data-cloud/database-sqlite?view=net-maui-8.0
    public class LocalDbService
    {
        private readonly SQLiteAsyncConnection _connection;

        public LocalDbService()
        {
            _connection = new SQLiteAsyncConnection(DbConstants.DatabasePath, DbConstants.Flags);
        }

        // ** Term **
        public async Task<CreateTableResult> InitTermTableAsync()
        {
            return await _connection.CreateTableAsync<Term>();
        }

        public async Task<List<Term>> GetTermsAsync()
        {
            await InitTermTableAsync();
            return await _connection.Table<Term>().ToListAsync();
        }

        public async Task<Term?> GetTermAsyncByIdAsync(int id)
        {
            await InitTermTableAsync();
            return await _connection.Table<Term>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<int> InsertTermAsync(Term term)
        {
            await InitTermTableAsync();
            return await _connection.InsertAsync(term);
        }

        public async Task<int> UpdateTermAsync(Term term)
        {
            await InitTermTableAsync();
            return await _connection.UpdateAsync(term);
        }

        public async Task<int> DeleteTermAsync(Term term)
        {
            await InitTermTableAsync();
            return await _connection.DeleteAsync(term);
        }
        // ** Term To Courses **
        public async Task<CreateTableResult> InitTermToCoursesTableAsync()
        {
            return await _connection.CreateTableAsync<TermToCourses>();
        }

        public async Task<List<TermToCourses>> GetCoursesIdPerTermAsync(int termId)
        {
            await InitTermToCoursesTableAsync();
            return await _connection.Table<TermToCourses>().Where(termToCourses =>  termToCourses.TermId == termId).ToListAsync();
        }

        public async Task<int> InsertCourseinTermAsync(TermToCourses termToCourses)
        {
            await InitTermToCoursesTableAsync();
            return await _connection.InsertAsync(termToCourses);
        }

        public async Task<int> DeleteCourseintoTermAsync(TermToCourses termToCourses)
        {
            await InitTermToCoursesTableAsync();
            return await _connection.DeleteAsync(termToCourses);
        }

        // ** Courses **
        public async Task<CreateTableResult> InitCourseTableAsync()
        {
            return await _connection.CreateTableAsync<Course>();
        }

        public async Task<Course?> GetCourseAsyncById(int id)
        {
            await InitCourseTableAsync();
            return await _connection.Table<Course>().Where(course => course.Id == id).FirstOrDefaultAsync();
        }

        public async Task<Course?> GetCourseLastRowIdAsync()
        {
            await InitCourseTableAsync();
            List<Course>? courses = null;
            int lastIdx = -1;
            await Task.Run(async () => courses = await _connection.Table<Course>().ToListAsync());
            
            if (courses is not null)
            {
                lastIdx = courses.Count - 1;
                return await _connection.Table<Course>().ElementAtAsync(lastIdx);
            } else
            {
                return null;
            }
        }

        public async Task<int> InsertCourseAsync(Course course)
        {
            await InitCourseTableAsync();
            return await _connection.InsertAsync(course);
        }

        public async Task<int> UpdateCourseAsync(Course course)
        {
            await InitCourseTableAsync();
            return await _connection.UpdateAsync(course);
        }

        public async Task<int> DeleteCourseAsync(Course course)
        {
            await InitCourseTableAsync();
            return await _connection.DeleteAsync(course);
        }

        // ** Instructor **
        public async Task<CreateTableResult> InitInstructorTableAsync()
        {
            return await _connection.CreateTableAsync<Instructor>();
        }

        public async Task<Instructor?> GetInstructorByCourseIdAsync(int courseId)
        {
            await InitInstructorTableAsync();
            return await _connection.Table<Instructor>().Where(instructor => instructor.fKCourseId == courseId).FirstOrDefaultAsync();
        }

        public async Task<Instructor?> GetInstructorLastRowIdAsync()
        {
            await InitAssessmentTableAsync();
            List<Instructor>? insctructors = null;
            int lastIdx = -1;
            await Task.Run(async () => insctructors = await _connection.Table<Instructor>().ToListAsync());

            if (insctructors is not null)
            {
                lastIdx = insctructors.Count - 1;
                return await _connection.Table<Instructor>().ElementAtAsync(lastIdx);
            }
            else
            {
                return null;
            }
        }

        public async Task<int> InsertInstructorAsync(Instructor instructor)
        {
            await InitInstructorTableAsync();
            return await _connection.InsertAsync(instructor);
        }

        public async Task<int> UpdateInstructorAsync(Instructor instructor)
        {
            await InitInstructorTableAsync();
            return await _connection.UpdateAsync(instructor);
        }

        public async Task<int> DeleteInsctructorAsync(Instructor instructor)
        {
            await InitInstructorTableAsync();
            return await _connection.DeleteAsync(instructor);
        }


        // ** Assessment **
        public async Task<CreateTableResult> InitAssessmentTableAsync()
        {
            return await _connection.CreateTableAsync<Assessment>();
        }

        public async Task<Assessment> GetAssessmentByCourseIdAssessmentTypeAsync(int courseId, AssessmentType assessmentType)
        {
            int type = Convert.ToInt32(assessmentType);
            await InitAssessmentTableAsync();
            return await _connection.Table<Assessment>()
                .Where(assessment => assessment.fKCourseId == courseId && assessment.Type == type).FirstOrDefaultAsync();
        }

        public async Task<Assessment?> GetAssessmentLastRowIdAsync()
        {
            await InitAssessmentTableAsync();
            List<Assessment>? assessments = null;
            int lastIdx = -1;
            await Task.Run(async () => assessments = await _connection.Table<Assessment>().ToListAsync());

            if (assessments is not null)
            {
                lastIdx = assessments.Count - 1;
                return await _connection.Table<Assessment>().ElementAtAsync(lastIdx);
            }
            else
            {
                return null;
            }
        }

        public async Task<int> InsertAssessmentAsync(Assessment assessment)
        {
            await InitAssessmentTableAsync();
            return await _connection.InsertAsync(assessment);
        }

        public async Task<int> UpdateAssessmentAsync(Assessment assessment)
        {
            await InitAssessmentTableAsync();
            return await _connection.UpdateAsync(assessment);
        }

        public async Task<int> DeleteAssessmentAsync(Assessment assessment)
        {
            await InitAssessmentTableAsync();
            return await _connection.DeleteAsync(assessment);
        }
    }
}
