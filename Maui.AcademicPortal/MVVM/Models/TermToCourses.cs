﻿using SQLite;

namespace Maui.AcademicPortal.MVVM.Models
{
    [Table("term_to_courses")]
    public class TermToCourses
    {
        [PrimaryKey]
        [AutoIncrement]
        [Unique]
        [Column("id")]
        public int Id { get; set; }

        [Column("term_id")]
        [NotNull]
        public int TermId { get; set; }

        [Column("course_id")]
        [NotNull]
        public int CourseId { get; set; }
    }
}
