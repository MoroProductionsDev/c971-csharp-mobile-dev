﻿using Maui.AcademicPortal.MVVM.Models.Storage;

namespace Maui.AcademicPortal.MVVM.Models
{
    public class Portal
    {
        // -------------------------------------------
        private LocalDbService _dbService = ((App)App.Current!).DbService;
        private List<Term> _terms = new();

        public Portal()
        {
            InitTerms();
        }

        public async void InitTerms()
        {
            await GetTerms();
        }

        public List<Term> Terms { get => _terms; }
                

        public async Task GetTerms()
        {
            // ** DB Query ** || _terms mutation
            await Task.Run(async () => _terms = await _dbService.GetTermsAsync());
        }

        public async Task AddTerm(Term term)
        {
            int affectedRows = 0;

            // ** DB Write ** || affectedRows mutation
            await Task.Run(async () => affectedRows = await _dbService.InsertTermAsync(term));

            // Logic
            if (affectedRows > 0)
            {
                this._terms.Add(term);
            }
        }

        public async Task EditTerm(Term term, int idx)
        {
            int affectedRows = 0;

            if (idx < 0 || idx >= _terms.Count)
            {
                return;
            }

            // ** DB Write ** || affectedRows mutation
            await Task.Run(async () => affectedRows = await _dbService.UpdateTermAsync(term));

            // Logic
            if (affectedRows > 0)
            {
                this._terms[idx] = term;
            }
        }

        public async Task RemoveTerm(Term term, int idx)
        {
            int affectedRows = 0;

            if (idx < 0 || idx >= _terms.Count)
            {
                return;
            }

            // ** DB Write ** || affectedRows mutation
            await Task.Run(async () => affectedRows = await _dbService.DeleteTermAsync(term));

            // Logic
            if (affectedRows > 0)
            {
                this._terms.RemoveAt(idx);
            }            
        }
    }
}