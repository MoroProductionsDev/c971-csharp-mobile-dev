﻿using SQLite;
using Maui.AcademicPortal.MVVM.Models.Storage;

namespace Maui.AcademicPortal.MVVM.Models
{
    [Table("term")]
    public class Term
    {
        [PrimaryKey]
        [AutoIncrement]
        [Column("id")]
        public int Id { get; set; }

        [Column("title")]
        [NotNull]
        public String Title { get; set; } = String.Empty;

        [Column("start_date")]
        [NotNull]
        public DateTime StartDate { get; set; } = DateTime.Now;

        [Column("end_date")]
        [NotNull]
        public DateTime EndDate { get; set; } = DateTime.Now;
        
        private LocalDbService _dbService = ((App)App.Current!).DbService;

        private List<Course> _courses = new();

        public Term()
        {
            Courses.Capacity = 6;
            InitCourses();
        }

        public async void InitCourses()
        {
            await GetCourses();
        }

        public async void InitCourses(int otherTermId)
        {
            await GetCourses(otherTermId);
        }

        public Term(Term anotherTerm)
        {
            Courses.Capacity = 6;

            this.Title = anotherTerm.Title;
            this.StartDate = anotherTerm.StartDate;
            this.EndDate = anotherTerm.EndDate;

            // shallow copy | same courses
            InitCourses(anotherTerm.Id);
        }

        public List<Course> Courses { get => _courses; }
        
        public async Task GetCourses()
        {
            List<TermToCourses>? termToCourses = null;


            await Task.Run(async () => termToCourses = await _dbService.GetCoursesIdPerTermAsync(this.Id));
           
            if (termToCourses is null) {
                return;
            }

            _courses.Clear();
            foreach (var termCourseRow in termToCourses)
            {

                Course? fetchedCourse = null;

                // ** DB Query **
                await Task.Run(async () => fetchedCourse = await GetCourseJoinedData(termCourseRow.CourseId));

                if (fetchedCourse is not null)
                {
                    _courses.Add(fetchedCourse);
                }
            }
        }

        private async Task GetCourses(int anotherTermId)
        {
            List<TermToCourses>? termToCourses = null;

            await Task.Run(async () => termToCourses = await _dbService.GetCoursesIdPerTermAsync(anotherTermId));

            if (termToCourses is null)
            {
                return;
            }

            _courses.Clear();
            foreach (var termCourseRow in termToCourses)
            {
                Course? fetchedCourse = null;

                // ** DB Query **
                await Task.Run(async () => fetchedCourse = await GetCourseJoinedData(termCourseRow.CourseId));

                if (fetchedCourse is not null)
                {
                    _courses.Add(fetchedCourse);
                }
            }
        }

        private async Task<Course?> GetCourseJoinedData(int id)
        {
            Course? fetchedCourse = null;
            Instructor? fetchedInstructor = null;
            Assessment? fetchedObjectiveAssessment = null;
            Assessment? fetchedPerformanceAssessment = null;


            await Task.Run(async () => fetchedCourse = await _dbService.GetCourseAsyncById(id));

            if (fetchedCourse is not null)
            {
                // ** DB Write ** || affectedRows mutation
                await Task.Run(async () => fetchedInstructor = await _dbService.GetInstructorByCourseIdAsync(fetchedCourse.Id));
                await Task.Run(async () => fetchedObjectiveAssessment = await _dbService.GetAssessmentByCourseIdAssessmentTypeAsync(fetchedCourse.Id, AssessmentType.Objective));
                await Task.Run(async () => fetchedPerformanceAssessment = await _dbService.GetAssessmentByCourseIdAssessmentTypeAsync(fetchedCourse.Id, AssessmentType.Performance));
            }

            if (fetchedCourse is not null &&
                fetchedInstructor is not null)
            {
                fetchedCourse.Instructor = fetchedInstructor;
                fetchedCourse.ObjectiveAssessment = fetchedObjectiveAssessment;
                fetchedCourse.PerformanceAssessment = fetchedPerformanceAssessment;
            }

            return fetchedCourse;
        }

        public async Task AddCourse(Course toBeAddedCourse)
        {
            Instructor? insertedInsctructor = null;
            Assessment? insertedObjective = null;
            Assessment? insertedPerformance = null;
            Course? insertedCourse = null;
            TermToCourses newTermToCourse = new TermToCourses();

            int affectedRows_Course = 0;
            int affectedRows_TermToCourse = 0;

            // ** DB Write ** || affectedRows mutation
            await Task.Run(async () => affectedRows_Course = await _dbService.InsertCourseAsync(toBeAddedCourse));
            // ** DB Query ** || affectedRows mutation
            await Task.Run(async () => insertedCourse = await _dbService.GetCourseLastRowIdAsync());

            if (insertedCourse is not null &&
                affectedRows_Course > 0)
            {
                // Other Tables FK - PK ConstraintRelationship (Course Id)
                toBeAddedCourse.Instructor.fKCourseId = insertedCourse.Id;

                if (toBeAddedCourse.ObjectiveAssessment is not null)
                {
                    toBeAddedCourse.ObjectiveAssessment.fKCourseId = insertedCourse.Id;
                    toBeAddedCourse.ObjectiveAssessment.fKCourseId = insertedCourse.Id;
                }

                if (toBeAddedCourse.PerformanceAssessment is not null)
                {
                    toBeAddedCourse.PerformanceAssessment.fKCourseId = insertedCourse.Id;
                }

                // ** DB Write ** || affectedRows mutation
                await Task.Run(async () => insertedInsctructor = await toBeAddedCourse.AddInstructor(toBeAddedCourse.Instructor));

                if (toBeAddedCourse.ObjectiveAssessment is not null)
                {
                    await Task.Run(async () => insertedObjective = await toBeAddedCourse.AddObjectiveAssessment(toBeAddedCourse.ObjectiveAssessment));
                }

                if (toBeAddedCourse.PerformanceAssessment is not null)
                {
                    await Task.Run(async () => insertedPerformance = await toBeAddedCourse.AddPerformanceAssessment(toBeAddedCourse.PerformanceAssessment));
                }

                if (insertedInsctructor is not null &&
                    insertedObjective is not null &&
                    insertedPerformance is not null)
                {
                    // Other Tables FK - PK ConstraintRelationship (Course Id)
                    insertedCourse.fK_InstructorId = insertedInsctructor.Id;
                    insertedCourse.fK_ObjectiveAsssessmentId = insertedObjective.Id;
                    insertedCourse.fK_PerformanceAsssessmentId = insertedPerformance.Id;

                    // ** DB Write ** || affectedRows mutation
                    await Task.Run(async () => affectedRows_Course = await _dbService.UpdateCourseAsync(insertedCourse));

                    // Logic | No DB write needed
                    insertedCourse.Instructor = insertedInsctructor;
                    insertedCourse.ObjectiveAssessment = insertedObjective;
                    insertedCourse.PerformanceAssessment = insertedPerformance;

                    // TermToCourses TableDate
                    newTermToCourse.TermId = this.Id;
                    newTermToCourse.CourseId = insertedCourse.Id;

                    // ** DB Write ** || affectedRows mutation
                    await Task.Run(async () => affectedRows_TermToCourse = await _dbService.InsertCourseinTermAsync(newTermToCourse));

                    if (affectedRows_TermToCourse > 0)
                    {
                        // Logic
                        this._courses.Add(insertedCourse);
                    }
                }
            }
        }

        public async Task EditCourse(Course course, int idx)
        {
            Course? foundCourse = null;

            if (idx < 0 || idx >= _courses.Count)
            {
                return;
            }

            // ** DB Query ** || affectedRows mutation
            await Task.Run(async () => foundCourse = await _dbService.GetCourseAsyncById(course.Id));

            if (foundCourse is not null)
            {
                Instructor? foundInsctructor = null;
                Assessment? foundObjectiveAssessment = null;
                Assessment? foundPerformanceAssessment = null;
                int affectedRows_Course = 0;

                // ** DB Write ** || affectedRows mutation
                await Task.Run(async () => foundInsctructor = await course.EditInstructor(course.Instructor, foundCourse.Id));
                await Task.Run(async () => foundObjectiveAssessment = await course.EditObjectiveAssessment(course.ObjectiveAssessment, foundCourse.Id));
                await Task.Run(async () => foundPerformanceAssessment = await course.EditPerformanceAssessment(course.PerformanceAssessment, foundCourse.Id));
                await Task.Run(async () => affectedRows_Course = await _dbService.UpdateCourseAsync(course));

                if (foundInsctructor is not null &&
                    affectedRows_Course > 0)
                {
                    // Logic
                    this._courses[idx] = course;
                }
            }
        }

        public async Task RemoveCourse(Course course, int idx)
        {
            Course? foundCourse = null;

            if (idx < 0 || idx >= _courses.Count)
            {
                return;
            }

            // ** DB Query ** || affectedRows mutation
            await Task.Run(async () => foundCourse = await _dbService.GetCourseAsyncById(course.Id));

            if (foundCourse is not null)
            {
                int affectedRows_Insctructor = 0;
                int affectedRows_ObjectiveAssessment = 0;
                int affectedRows_PerformanceAssessment = 0;
                int affectedRows_Course = 0;
                int affectedRows_TermToCourses = 0;

                // ** DB Write ** || affectedRows mutation
                await Task.Run(async () => affectedRows_Insctructor = await course.RemoveInstructor(course.Instructor, foundCourse.Id));
                await Task.Run(async () => affectedRows_ObjectiveAssessment = await course.RemoveObjectiveAssessment(course.ObjectiveAssessment, foundCourse.Id));
                await Task.Run(async () => affectedRows_PerformanceAssessment = await course.RemovePerformanceAssessment(course.PerformanceAssessment, foundCourse.Id));
                await Task.Run(async () => affectedRows_Course = await _dbService.DeleteCourseAsync(course));

                // Term To Courses
                List<TermToCourses>? termToCourses = null;

                // ** DB Query ** || affectedRows mutation
                await Task.Run(async () => termToCourses = await _dbService.GetCoursesIdPerTermAsync(this.Id));
                // Find the one for the course
                TermToCourses? termToCourse = termToCourses?.Where(termToCourses => termToCourses.CourseId == course.Id).FirstOrDefault();

                if (termToCourse is not null)
                {
                    // ** DB Write ** || affectedRows mutation
                    await Task.Run(async () => affectedRows_TermToCourses = await _dbService.DeleteCourseintoTermAsync(termToCourse));
                }
               

                if (affectedRows_Insctructor > 0 &&
                    affectedRows_ObjectiveAssessment > 0 &&
                    affectedRows_PerformanceAssessment > 0 &&
                    affectedRows_Course > 0 &&
                    affectedRows_TermToCourses > 0)
                {
                    // Logic
                    this._courses.RemoveAt(idx);
                }
            }
        }
    }
}
