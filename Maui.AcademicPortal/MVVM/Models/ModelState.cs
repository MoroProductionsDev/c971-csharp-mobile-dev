﻿namespace Maui.AcademicPortal.MVVM.Models
{
    public class ModelState
    {
        public Portal PortalModel { get; } = new();
        public const int MAX_COURSE_PER_SEMESTER = 6;
    }
}
