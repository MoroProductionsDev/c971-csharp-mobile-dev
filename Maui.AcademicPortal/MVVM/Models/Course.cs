﻿using SQLite;
using Maui.AcademicPortal.MVVM.Models.Storage;

namespace Maui.AcademicPortal.MVVM.Models
{
    [Table("course")]
    public class Course
    {
        [PrimaryKey]
        [AutoIncrement]
        [Unique]
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        [NotNull]
        public String Name { get; set; } = String.Empty;

        [Column("status")]
        [NotNull]
        public CourseStatusEnum Status { get; set; } = CourseStatusEnum.Scheduled;

        [Column("note")]
        [NotNull]
        public String Note { get; set; } = String.Empty;

        [Column("start_date")]
        [NotNull]
        public DateTime StartDate { get; set; } = DateTime.Now;

        [Column("end_date")]
        [NotNull]
        public DateTime EndDate { get; set; } = DateTime.Now;

        [Column("fk_instructor_id")]
        [NotNull]
        public int fK_InstructorId { get; set; }

        [Column("fk_objective_asssessment_id")]
        public int fK_ObjectiveAsssessmentId{ get; set; }

        [Column("fk_performance_asssessment_id")]
        public int fK_PerformanceAsssessmentId { get; set; }

        [Ignore]
        public Instructor Instructor { get; set; } = new();
        [Ignore]
        public Assessment? ObjectiveAssessment { get; set; } = new();
        [Ignore]
        public Assessment? PerformanceAssessment { get; set; } = new();

        private LocalDbService _dbService = ((App)App.Current!).DbService;

        public Course()
        {
            
        }

        public Course(Course anotherCourse)
        {
            this.Name = anotherCourse.Name;
            this.Status = anotherCourse.Status;
            this.Note = anotherCourse.Note;
            this.StartDate = anotherCourse.StartDate;
            this.EndDate = anotherCourse.EndDate;

            this.Instructor = anotherCourse.Instructor;
            this.ObjectiveAssessment = anotherCourse.ObjectiveAssessment;
            this.PerformanceAssessment = anotherCourse.PerformanceAssessment;
        }

        // ** Instructor **
        public async Task<Instructor?> AddInstructor(Instructor toBeInsertedinstructor)
        {
            Instructor? insertedInsctructor = null;

            int affectedRows = 0;

            // ** DB Write ** || affectedRows mutation
            await Task.Run(async () => affectedRows = await _dbService.InsertInstructorAsync(toBeInsertedinstructor));
            // ** DB Query ** || affectedRows mutation
            await Task.Run(async () => insertedInsctructor = await _dbService.GetInstructorLastRowIdAsync());

            // Logic
            if (insertedInsctructor is not null && 
                affectedRows > 0)
            {
                return insertedInsctructor;
            }

            return null;
        }

        public async Task<Instructor?> EditInstructor(Instructor instructor, int courseId)
        {
            Instructor? foundInsctrutor = null;
            int affectedRows = 0;

            // ** DB Query **
            await Task.Run(async () => foundInsctrutor = await _dbService.GetInstructorByCourseIdAsync(courseId));

            if (foundInsctrutor is not null)
            {
                // ** DB Write ** || affectedRows mutation
                await Task.Run(async () => affectedRows = await _dbService.UpdateInstructorAsync(instructor));
                
                // Logic
                if (affectedRows > 0)
                {
                    // Assign Object
                    return foundInsctrutor;
                }
            }
            return null;
        }

        public async Task<int> RemoveInstructor(Instructor instructor, int courseId)
        {
            Instructor? foundInsctrutor = null;
            int affectedRows = 0;

            // ** DB Query **
            await Task.Run(async () => foundInsctrutor = await _dbService.GetInstructorByCourseIdAsync(courseId));

            // Logic
            if (foundInsctrutor is not null)
            {
                // ** DB Write ** || affectedRows mutation
                await Task.Run(async () => affectedRows = await _dbService.DeleteInsctructorAsync(instructor));
            }

            return affectedRows;
        }

        // ** ObjectiveAssessment **

        public async Task<Assessment?> AddObjectiveAssessment(Assessment assessment)
        {
            Assessment? insertedAssessment = null;
            int affectedRows = 0;

            // Assessment Type
            assessment.Type = Convert.ToInt32(AssessmentType.Objective);

            // ** DB Write **
            await Task.Run(async () => affectedRows = await _dbService.InsertAssessmentAsync(assessment));
            // ** DB Query **
            await Task.Run(async () => insertedAssessment = await _dbService.GetAssessmentLastRowIdAsync());

            // Logic
            if (insertedAssessment is not null &&
                affectedRows > 0)
            {
                return insertedAssessment;
            }

            return null;
        }

        public async Task<Assessment?> EditObjectiveAssessment(Assessment? assessment, int courseId)
        {
            Assessment? foundAssessment = null;
            int affectedRows = 0;

            if (assessment is null)
            {
                return null;
            }

            // ** DB Query **
            await Task.Run(async () => foundAssessment = await _dbService.GetAssessmentByCourseIdAssessmentTypeAsync(courseId, AssessmentType.Objective));

            if (foundAssessment is not null)
            {
                // ** DB Write ** || affectedRows mutation
                await Task.Run(async () => affectedRows = await _dbService.UpdateAssessmentAsync(assessment));

                // Logic
                if (affectedRows > 0)
                {
                    return foundAssessment;
                }
            }

            return null;
        }

        public async Task<int> RemoveObjectiveAssessment(Assessment? assessment, int courseId)
        {
            Assessment? foundAssessment = null;
            int affectedRows = 0;

            if (assessment is null)
            {
                return 0;
            }

            // ** DB Query **
            await Task.Run(async () => foundAssessment = await _dbService.GetAssessmentByCourseIdAssessmentTypeAsync(courseId, AssessmentType.Objective));

            // Logic
            if (foundAssessment is not null)
            {
                // ** DB Write ** || affectedRows mutation
                await Task.Run(async () => affectedRows = await _dbService.DeleteAssessmentAsync(assessment));
            }

            return affectedRows;
        }

        // ** PerformanceAssessment **

        public async Task<Assessment?> AddPerformanceAssessment(Assessment assessment)
        {
            Assessment? insertedAssessment = null;
            int affectedRows = 0;

            assessment.Type = Convert.ToInt32(AssessmentType.Performance);

            // ** DB Write **
            await Task.Run(async () => affectedRows = await _dbService.InsertAssessmentAsync(assessment));
            // ** DB Query **
            await Task.Run(async () => insertedAssessment = await _dbService.GetAssessmentLastRowIdAsync());


            // Logic
            if (insertedAssessment is not null &&
                affectedRows > 0)
            {
                return insertedAssessment;
            }

            return null;
        }

        public async Task<Assessment?> EditPerformanceAssessment(Assessment? assessment, int courseId)
        {
            Assessment? foundAssessment = null;
            int affectedRows = 0;

            if (assessment is null)
            {
                return null;
            }

            // ** DB Query **
            await Task.Run(async () => foundAssessment = await _dbService.GetAssessmentByCourseIdAssessmentTypeAsync(courseId, AssessmentType.Performance));

            if (foundAssessment is not null)
            {
                // ** DB Write ** || affectedRows mutation
                await Task.Run(async () => affectedRows = await _dbService.UpdateAssessmentAsync(assessment));

                // Logic
                if (affectedRows > 0)
                {
                    return foundAssessment;
                }
            }

            return null;
        }

        public async Task<int> RemovePerformanceAssessment(Assessment? assessment, int courseId)
        {
            Assessment? foundAssessment = null;
            int affectedRows = 0;

            if (assessment is null)
            {
                return 0;
            }

            // ** DB Query **
            await Task.Run(async () => foundAssessment = await _dbService.GetAssessmentByCourseIdAssessmentTypeAsync(courseId, AssessmentType.Performance));

            if (foundAssessment is not null)
            {
                // ** DB Write ** || affectedRows mutation
                await Task.Run(async () => affectedRows = await _dbService.DeleteAssessmentAsync(assessment));
            }

            return affectedRows;
        }
    }

    [Table("instructor")]
    public class Instructor
    {

        [PrimaryKey]
        [AutoIncrement]
        [Unique]
        [Column("id")]
        public int Id { get; set; }

        [Column("email")]
        [NotNull]
        public String Email { get; set; } = String.Empty;

        [Column("name")]
        [NotNull]
        public String Name { get; set; } = String.Empty;

        [Column("phone")]
        [NotNull]
        public String Phone { get; set; } = String.Empty;

        [Column("fk_course_id")]
        [NotNull]
        public int fKCourseId { get; set; }

        public Instructor()
        {

        }

        public Instructor(Instructor anotherInstructor)
        {
            this.Name = anotherInstructor.Name;
            this.Email = anotherInstructor.Email;
            this.Phone = anotherInstructor.Phone;
        }
    }

    [Table("assessment")]
    public class Assessment
    {
        [PrimaryKey]
        [AutoIncrement]
        [Unique]
        [Column("id")]
        public int Id { get; set; }

        [Column("type")]
        public int Type { get; set; }

        [Column("name")]
        [NotNull]
        public String Name { get; set; } = String.Empty;

        [Column("start_date")]
        [NotNull]
        public DateTime StartDate { get; set; } = DateTime.Now;

        [Column("end_date")]
        [NotNull]
        public DateTime EndDate { get; set; } = DateTime.Now;


        [Column("fk_course_id")]
        [NotNull]
        public int fKCourseId { get; set; }

        public Assessment()
        {

        }

        public Assessment(Assessment anotherAssessment)
        {
            this.Id = anotherAssessment.Id;
            this.Type = anotherAssessment.Type;
            this.StartDate = anotherAssessment.StartDate;
            this.EndDate = anotherAssessment.EndDate;
        }
    }

    public enum CourseStatusEnum {
        Scheduled = 0,
        Progress = 1,
        Completed = 2,
        Dropped = 3,
    }

    public enum AssessmentType
    {
        Objective = 0,
        Performance = 1
    }

    public static class CourseStateStr
    {
        public const string Scheduled = "Scheduled";
        public const string Progress = "Progress";
        public const string Completed = "Completed";
        public const string Dropped = "Dropped";
    }
}


