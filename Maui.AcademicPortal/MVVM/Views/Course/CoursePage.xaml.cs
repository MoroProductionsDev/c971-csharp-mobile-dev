using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Course.Landscape;
using Maui.AcademicPortal.MVVM.Views.Course.Portrait;

namespace Maui.AcademicPortal.MVVM.Views.Course;

public partial class CoursePage : ContentPage
{
    private readonly CoursePortraitViewPage courseViewPortrait = new();
	private readonly CourseLandscapeViewPage courseViewLandscape = new();

    public CoursePage()
	{
        InitializeComponent();
        EventSubscriptions();

        this.Content = courseViewPortrait;
    }
    public void Dispose()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged -= Current_MainDisplayInfoChanged!;
        this.Loaded -= CoursePage_Loaded;
        ViewActions.OnSuccesfulCourseSubmission -= UpdateBinding;
    }

    private void EventSubscriptions()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged += Current_MainDisplayInfoChanged!;
        this.Loaded += CoursePage_Loaded;
        ViewActions.OnSuccesfulCourseSubmission += UpdateBinding;
    }

    private void CoursePage_Loaded(object? sender, EventArgs e)
    {
        ViewActions.ResetProcessTrigger(sender: this);
        
        SelectableCourseViewModel? selectedCourseViewModel = ((App)App.Current!).ViewModelState.SelectableCourseViewModel;
        if (selectedCourseViewModel is not null)
        {
            this.courseViewPortrait.BindingContext = selectedCourseViewModel;
            this.courseViewLandscape.BindingContext = selectedCourseViewModel;
        }
    }
    private void UpdateBinding(object? sender, EventArgs e)
    {
        SelectableCourseViewModel? selectableCourseViewModel = ((App)App.Current!).ViewModelState.SelectableCourseViewModel;
        if (selectableCourseViewModel is not null)
        {
            this.courseViewPortrait.BindingContext = selectableCourseViewModel;
            this.courseViewLandscape.BindingContext = selectableCourseViewModel;
        }
    }

    private void Current_MainDisplayInfoChanged(object sender, DisplayInfoChangedEventArgs e)
	{	
		ContentView? currentView = ViewProvider.GetNewOrientedPage(e.DisplayInfo.Orientation,
											new ContentView[]{ courseViewPortrait, courseViewLandscape});

		if (currentView != null)
		{
			this.Content = currentView;
		}
    }
}
