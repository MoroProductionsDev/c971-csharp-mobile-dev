using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.ViewProcesses;

namespace Maui.AcademicPortal.MVVM.Views.Course.ContentViews;

public partial class CourseAssessmentsView : ContentView
{
    private ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public CourseAssessmentsView()
	{
		InitializeComponent();
    }

    // Reference: https://github.com/thudugala/Plugin.LocalNotification/discussions/359
    private async void AssessmentsDateToggle_Toggled(object sender, ToggledEventArgs e)
    {
        SelectableCourseViewModel? selectedCourseViewModel = viewModalState.SelectableCourseViewModel;

        if (selectedCourseViewModel is not null && 
            selectedCourseViewModel.CurrentCourse.ObjectiveAssessment is not null)
        {
            const int objectiveNotificationId = 101;
            DateTime objectiveStartDate = selectedCourseViewModel.CurrentCourse.ObjectiveAssessment.StartDate;
            DateTime objectiveEndDate = selectedCourseViewModel.CurrentCourse.ObjectiveAssessment.EndDate;

            await Task.Run(() => Notification.RunDateNotication(objectiveNotificationId, "Objective Assessment Reminder", objectiveStartDate, objectiveEndDate));
        }

        if (selectedCourseViewModel is not null &&
            selectedCourseViewModel.CurrentCourse.PerformanceAssessment is not null)
        {
            const int performanceNotificationId = 102;
            DateTime performanceStartDate = selectedCourseViewModel.CurrentCourse.PerformanceAssessment.StartDate;
            DateTime performanceEndDate = selectedCourseViewModel.CurrentCourse.PerformanceAssessment.EndDate;

            await Task.Run(() => Notification.RunDateNotication(performanceNotificationId, "Performance Assessment Reminder", performanceStartDate, performanceEndDate));
        }
    }

    private async void DeleteObjectiveAssessmentBtn_Clicked(object sender, EventArgs e)
    {
        SelectableTermViewModel? selectedTermViewModel = viewModalState.SelectableTermViewModel;
        SelectableCourseViewModel? selectedCourseViewModel = viewModalState.SelectableCourseViewModel;
        if (selectedTermViewModel is null || selectedCourseViewModel is null ||
            selectedCourseViewModel.CurrentCourse.ObjectiveAssessment is null)
        {
            return;
        }

        // UI
        var result = await App.Current!.MainPage!.DisplayAlert("Delete Objective Assessment", $"Are you sure you want to delete this assessment [{selectedCourseViewModel.CurrentCourse.ObjectiveAssessment.Name}]?", "Yes", "Cancel");


        if (result)
        {

            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.deleteObjectiveAssessmentBtn);

            // Logic
            await Task.Run(async () =>
            {
                await selectedCourseViewModel.CurrentCourse
                    .RemoveObjectiveAssessment(selectedCourseViewModel.CurrentCourse.ObjectiveAssessment, selectedCourseViewModel.CurrentCourse.Id);
                selectedCourseViewModel.CurrentCourse.ObjectiveAssessment = null;
                selectedCourseViewModel.HasObjectiveAssessment = false;

                // ** Await Database **
                await selectedTermViewModel.FetchCourses();
            });

            // UI
            ViewActions.ResetProcessTrigger(sender: this);
            await Navigation.PopAsync();

            // Must be call as the last one.
            ViewActions.OnSuccesfulCourseSubmission?.Invoke(this, EventArgs.Empty);
        }
    }


    private async void DeletePerformanceAssessmentBtn_Clicked(object sender, EventArgs e)
    {
        SelectableTermViewModel? selectedTermViewModel = viewModalState.SelectableTermViewModel;
        SelectableCourseViewModel? selectedCourseViewModel = viewModalState.SelectableCourseViewModel;
        if (selectedTermViewModel is null ||
            selectedCourseViewModel is null ||
            selectedCourseViewModel.CurrentCourse.PerformanceAssessment is null)
        {
            return;
        }

        // UI
        var result = await App.Current!.MainPage!.DisplayAlert("Delete Performance Assessment", $"Are you sure you want to delete this assessment [{selectedCourseViewModel.CurrentCourse.PerformanceAssessment.Name}]?", "Yes", "Cancel");


        if (result)
        {

            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.deletePerformanceAssessmentBtn);

            // Logic
            await Task.Run(async () =>
            {
                await selectedCourseViewModel.CurrentCourse
                    .RemovePerformanceAssessment(selectedCourseViewModel.CurrentCourse.PerformanceAssessment, selectedCourseViewModel.CurrentCourse.Id);
                    selectedCourseViewModel.CurrentCourse.PerformanceAssessment = null;
                    selectedCourseViewModel.HasPerformanceAssessment = false;

                // ** Await Database **
                await selectedTermViewModel.FetchCourses();
            });

            // UI
            ViewActions.ResetProcessTrigger(sender: this);
            await Navigation.PopAsync();

            // Must be call as the last one.
            ViewActions.OnSuccesfulCourseSubmission?.Invoke(this, EventArgs.Empty);
        }
    }
}