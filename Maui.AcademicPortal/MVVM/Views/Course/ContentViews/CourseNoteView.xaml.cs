using static System.Net.Mime.MediaTypeNames;

namespace Maui.AcademicPortal.MVVM.Views.Course.ContentViews;

public partial class CourseNoteView : ContentView
{
	public CourseNoteView()
	{
		InitializeComponent();
	}

    public async Task ShareText(string title, string text)
    {
        await Share.Default.RequestAsync(new ShareTextRequest
        {
            Title =title,
            Text = text,
        });
    }

    private async void ShareImgBtn_Clicked(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.noteLbl.Text))
        {
            await Task.Run(() => ShareText(title: "Share Course Note", text: this.noteLbl.Text));
        }
        else
        {
            App.Current?.MainPage?.DisplayAlert("Empty Note", "There is no note to share.", "Ok");
        }
    }
}