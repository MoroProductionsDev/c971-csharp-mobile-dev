using Maui.AcademicPortal.MVVM.Views.Course_Edit;
using Maui.AcademicPortal.MVVM.ViewModels;

namespace Maui.AcademicPortal.MVVM.Views.Course.ContentViews;

public partial class CourseActionsView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public CourseActionsView()
	{
		InitializeComponent();
	}

    private async void EditBtn_Clicked(object sender, EventArgs e)
    {
        ViewActions.StartProcessTrigger(sender: this, triggeredView: this.editBtn);
        await Navigation.PushModalAsync(new CourseEditPage());
    }

    private async void DeleteBtn_Clicked(object sender, EventArgs e)
    {
        SelectableTermViewModel? selectableTermViewModel = viewModalState.SelectableTermViewModel;
        SelectableCourseViewModel? selectedCourseViewModel = viewModalState.SelectableCourseViewModel;

        if (selectableTermViewModel is null ||
            selectedCourseViewModel is null)
        {
            return;
        }

        // UI
        var result = await App.Current!.MainPage!.DisplayAlert("Delete Course", $"Are you sure you want to delete this course [{selectedCourseViewModel.Name}]?", "Yes", "Cancel");
       
        if (result)
        {

            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.deleteBtn);

            // Logic
            await Task.Run(async () =>
            {
                // ** Await Database **
                await selectableTermViewModel.RemoveCourse(selectedCourseViewModel.CurrentCourse, selectedCourseViewModel.SelectedIdx);
                await selectableTermViewModel.FetchCourses();
            });

            // UI
            ViewActions.ResetProcessTrigger(sender: this);
            await Navigation.PopAsync();

            // Must be call as the last one.
            ViewActions.OnSuccesfulCourseSubmission?.Invoke(this, EventArgs.Empty);
        }
    }
}