using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.ViewProcesses;

namespace Maui.AcademicPortal.MVVM.Views.Course.ContentViews;

public partial class CourseDatesView : ContentView
{
    public CourseDatesView()
	{
		InitializeComponent();
    }

    private async void CoursesDateNotificationToggle_Toggled(object? sender, ToggledEventArgs e)
    {
        SelectableCourseViewModel? selectableCourseViewModel = ((App)App.Current!).ViewModelState.SelectableCourseViewModel;
        if (selectableCourseViewModel is not null)
        {
            const int courseDateNotificationId = 100;
            DateTime startDate = selectableCourseViewModel.CurrentCourse.StartDate;
            DateTime endDate = selectableCourseViewModel.CurrentCourse.EndDate;

            await Task.Run(() => Notification.RunDateNotication(courseDateNotificationId, "Course Reminder", startDate, endDate));
        }
    }
}