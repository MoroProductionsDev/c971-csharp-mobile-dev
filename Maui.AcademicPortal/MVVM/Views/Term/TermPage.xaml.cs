using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Term.Landscape;
using Maui.AcademicPortal.MVVM.Views.Term.Portrait;

namespace Maui.AcademicPortal.MVVM.Views.Term;

public partial class TermPage : ContentPage
{
    private readonly TermPortraitViewPage termViewPortrait = new();
    private readonly TermLandscapeViewPage termViewLandscape = new();

    public TermPage()
    {
        InitializeComponent();
        EventSubscriptions();

        this.Content = termViewPortrait;
    }

    public void Dispose()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged -= Current_MainDisplayInfoChanged!;
        this.Loaded -= TermPage_Loaded;
    }

    private void EventSubscriptions()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged += Current_MainDisplayInfoChanged!;
        this.Loaded += TermPage_Loaded;
    }

    private void TermPage_Loaded(object? sender, EventArgs e)
    {
        ViewActions.ResetProcessTrigger(sender: this);

        SelectableTermViewModel? selectedTermViewModel = ((App)App.Current!).ViewModelState.SelectableTermViewModel;
        if (selectedTermViewModel is not null)
        {
            this.termViewPortrait.BindingContext = selectedTermViewModel;
            this.termViewLandscape.BindingContext = selectedTermViewModel;
        }
    }
    private void Current_MainDisplayInfoChanged(object sender, DisplayInfoChangedEventArgs e)
    {
        ContentView? currentView = ViewProvider.GetNewOrientedPage(e.DisplayInfo.Orientation,
                                            new ContentView[] { termViewPortrait, termViewLandscape });

        if (currentView != null)
        {
            this.Content = currentView;
        }
    }
}