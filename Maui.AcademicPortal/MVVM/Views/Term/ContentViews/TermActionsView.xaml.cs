using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Course_Add;
using Maui.AcademicPortal.MVVM.Views.Course_Edit;

namespace Maui.AcademicPortal.MVVM.Views.Term.ContentViews;

public partial class TermActionsView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public TermActionsView()
	{
		InitializeComponent();
        EventSubscriptions();
    }

    public void Dispose()
    {
        this.Loaded -= TermActionsView_Loaded;
    }

    private void EventSubscriptions()
    {
        this.Loaded += TermActionsView_Loaded;
    }

    private void TermActionsView_Loaded(object? sender, EventArgs e)
    {
        SelectableTermViewModel selectableTermViewModel = (SelectableTermViewModel) this.BindingContext;

        // UI
        if (selectableTermViewModel.Courses.Count <= TermViewModel.MAX_COURSES_PER_TERM)
        {
            this.addBtn.IsEnabled = true;
        }
        else
        {
            this.addBtn.IsEnabled = false;
        }
    }

    private async void AddCourseBtn_Clicked(object sender, EventArgs e)
    {
        // UI
        ViewActions.StartProcessTrigger(sender: this, triggeredView: this.addBtn);
        await Navigation.PushModalAsync(new CourseAddPage());
    }

    private async void EditCourseBtn_Clicked(object sender, EventArgs e)
    {
        SelectableCourseViewModel? selectableCourseViewModel = viewModalState.SelectableCourseViewModel;
        if (selectableCourseViewModel is not null)
        {
            // UI
            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.editBtn);
            await Navigation.PushModalAsync(new CourseEditPage());
        } else
        {
            // UI
            App.Current?.MainPage?.DisplayAlert("Unselected Course", "Please select a course to proceed.", "Ok");
        }
    }

    private async void DeleteCourseBtn_Clicked(object sender, EventArgs e)
    {
        SelectableTermViewModel selectableTermViewModel = (SelectableTermViewModel)this.BindingContext;
        SelectableCourseViewModel? selectedCourseViewModel = viewModalState.SelectableCourseViewModel;

        if (selectedCourseViewModel is null)
        {
            // UI
            App.Current?.MainPage?.DisplayAlert("Unselected Course", "Please select a course to proceed.", "Ok");
            return;
        }

        // UI
        var result = await App.Current!.MainPage!.DisplayAlert("Delete Course", $"Are you sure you want to delete this course [{selectedCourseViewModel.Name}]?", "Yes", "Cancel");

        if (result)
        {
            // UI
            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.deleteBtn);

            // Logic
            await Task.Run(async () =>
            {
                // ** Await Database **
                await selectableTermViewModel.RemoveCourse(selectedCourseViewModel.CurrentCourse, selectedCourseViewModel.SelectedIdx);
                await selectableTermViewModel.FetchCourses();
            });

            // UI
            ViewActions.ResetProcessTrigger(sender: this);

            // Must be call as the last one.
            ViewActions.OnSuccesfulCourseSubmission?.Invoke(this, EventArgs.Empty);
        }
    }
}