using Maui.AcademicPortal.MVVM.ViewModels;

namespace Maui.AcademicPortal.MVVM.Views.Term.ContentViews;

public partial class TermHeaderView : ContentView
{
    private readonly SelectableTermViewModel? selectableTermViewModel = ((App)App.Current!).ViewModelState.SelectableTermViewModel;
    public TermHeaderView()
    {
        InitializeComponent();
        EventSubscriptions();

        if (selectableTermViewModel is not null)
        {
            this.BindingContext = selectableTermViewModel;
        }
    }

    public void Dispose()
    {
        ViewActions.OnSuccesfulCourseSubmission -= UpdateCollectionView;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnSuccesfulCourseSubmission += UpdateCollectionView;
    }

    private void UpdateCollectionView(object? sender, EventArgs e)
    {
        if (selectableTermViewModel is not null)
        {
            this.coursesCountLbl.Text = $"({selectableTermViewModel.Courses.Count.ToString()}) Course(s)";
        }
    }
}