using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Course;

namespace Maui.AcademicPortal.MVVM.Views.Term.ContentViews;

public partial class TermCoursesView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public TermCoursesView()
	{
		InitializeComponent();
        EventSubscriptions();

        if (this.viewModalState.SelectableTermViewModel is not null)
        {
            var selectableTermViewModel = this.viewModalState.SelectableTermViewModel;
            this.coursesViewCollection.ItemsSource = selectableTermViewModel.Courses;
        }
    }

    public void Dispose()
    {
        ViewActions.OnSuccesfulCourseSubmission -= UpdateCollectionView;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnSuccesfulCourseSubmission += UpdateCollectionView;
    }

    private async void UpdateCollectionView(object? sender, EventArgs e)
    {
        await MainThread.InvokeOnMainThreadAsync(() =>
        {
            var selectableTermViewModel = this.viewModalState.SelectableTermViewModel;

            if (selectableTermViewModel is not null)
            {
                this.coursesViewCollection.ItemsSource = null;
                this.coursesViewCollection.ItemsSource = selectableTermViewModel.CurrentTerm.Courses;
            }
        });
    }

    private async void CourseViewCollection_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        // Logic
        await Task.Run(() =>
        {
            // Object Creation
            viewModalState.NewSelectableCourseViewModel(this.coursesViewCollection.ItemsSource.Cast<object>(), this.coursesViewCollection.SelectedItem);
        });

        await MainThread.InvokeOnMainThreadAsync(() =>
        {
            SelectableCourseViewModel? selectableCourseViewModel = this.viewModalState.SelectableCourseViewModel;
            selectableCourseViewModel!.SelectedCourse = this.coursesViewCollection.SelectedItem;

            // UI
            ViewActions.CVBtn_EnablingDisablingState(this.coursesViewCollection, selectableCourseViewModel.SelectedIdx);
        });
    }

    private async void ViewCourseBtn_Clicked(object sender, EventArgs e)
    {
        SelectableCourseViewModel? selectableCourseViewModel = this.viewModalState.SelectableCourseViewModel;

        if (selectableCourseViewModel is not null)
        {
            // UI
            var viewCourseBtns = coursesViewCollection.GetVisualTreeDescendants().Where(view => view.GetType() == typeof(Button)).ToArray();

            ViewActions.StartProcessTrigger(sender: this, triggeredView: ((Button)viewCourseBtns[selectableCourseViewModel.SelectedIdx]));
            await Navigation.PushAsync(new CoursePage());
        }
    }
}