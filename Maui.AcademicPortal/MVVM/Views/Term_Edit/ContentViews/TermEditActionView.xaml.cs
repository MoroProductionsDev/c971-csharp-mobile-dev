using Maui.AcademicPortal.MVVM.ViewModels;

namespace Maui.AcademicPortal.MVVM.Views.Term_Edit.ContentViews;

public partial class TermEditActionView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
	public TermEditActionView()
	{
		InitializeComponent();
	}

    private async void UpdateBtn_Clicked(object sender, EventArgs e)
    {
        // PROCESS AND VALIDATION
        // Logic
        PortalViewModel? mainPortalViewModel = viewModalState.PortalViewModel;
        ValidatableTermViewModel? ValidatableTermViewModel = viewModalState.ValidatableTermViewModel;
        TermViewModel? selectableTermViewModel = viewModalState.SelectableTermViewModel;

        if (mainPortalViewModel is not null &&
            ValidatableTermViewModel is not null &&
            selectableTermViewModel is not null &&
            ValidatableTermViewModel.CheckIfDataChange(selectableTermViewModel) &&
            ValidatableTermViewModel.Validate())
        {
            // UI
            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.updateBtn);

            // Logic
            await Task.Run(async () =>
            {
                ValidatableTermViewModel.CopyDataTo(selectableTermViewModel);

                int selectedIdx = ((SelectableTermViewModel)selectableTermViewModel).SelectedIdx;
                
                // ** Await Database **
                await mainPortalViewModel.EditTerm(selectableTermViewModel.CurrentTerm, selectedIdx);
                await mainPortalViewModel.FetchTerms();
            });

            await Navigation.PopModalAsync();
            ViewActions.ResetProcessTrigger(sender: this);

            // Must be call as the last one.
            ViewActions.OnSuccesfulTermSubmission?.Invoke(this, EventArgs.Empty);
        }
        else
        {  
            if (ValidatableTermViewModel is not null && !ValidatableTermViewModel.HasDataChanged)
            {
                App.Current?.MainPage?.DisplayAlert("Unchanged Data", "Data values has not changed.", "Ok");
            } else
            {
                ViewActions.OnAttemptedSubmission?.Invoke(this, EventArgs.Empty);
            }
        }
    }

    private async void CancelBtn_Clicked(object sender, EventArgs e)
    {
        ViewActions.StartProcessTrigger(sender: this, triggeredView: this.cancelBtn);
        await Navigation.PopModalAsync();
        ViewActions.ResetProcessTrigger(sender: this);
    }
}