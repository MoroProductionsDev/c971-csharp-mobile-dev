using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Validation;

namespace Maui.AcademicPortal.MVVM.Views.Term_Edit.ContentViews;

public partial class TermEditTitleAndDatesView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public TermEditTitleAndDatesView()
	{
		InitializeComponent();
        EventSubscriptions();
    }

    public void Dispose()
    {
        ViewActions.OnOrientationChanged -= ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission -= ValidateUIComponents!;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnOrientationChanged += ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission += ValidateUIComponents!;
    }

    private void ResumeBindingContext(object sender, EventArgs e)
    {
        // To work the BidingMode must be set to 'TwoWay'.
        this.BindingContext = ((App)App.Current!).ViewModelState.ValidatableTermViewModel;
    }

    private void TitleTxtField_TextChanged(object sender, TextChangedEventArgs e)
    {
        var editableTermViewModel = viewModalState.ValidatableTermViewModel;
        if (editableTermViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableTermViewModel.vTitle, errorLbl: this.titleErrorLbl);
        }
    }

    private void StartDatePicker_DateSelected(object sender, DateChangedEventArgs e)
    {
        var editableTermViewModel = viewModalState.ValidatableTermViewModel;
        if (editableTermViewModel is not null)
        {
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableTermViewModel.vStartDate, errorLbl: this.startErrorLbl);
        }
    }

    private void EndDatePicker_DateSelected(object sender, DateChangedEventArgs e)
    {
        var editableTermViewModel = viewModalState.ValidatableTermViewModel;
        if (editableTermViewModel is not null)
        {
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableTermViewModel.vEndDate, errorLbl: this.endErrorLbl);
        }
    }

    private void ValidateUIComponents(object sender, EventArgs e)
    {
        var editableTermViewModel = viewModalState.ValidatableTermViewModel;
        if (editableTermViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableTermViewModel.vTitle, errorLbl: this.titleErrorLbl);
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableTermViewModel.vStartDate, errorLbl: this.startErrorLbl);
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableTermViewModel.vEndDate, errorLbl: this.endErrorLbl);
        }
    }
}