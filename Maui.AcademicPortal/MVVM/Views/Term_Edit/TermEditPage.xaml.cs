using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Term_Edit.Portrait;
using Maui.AcademicPortal.MVVM.Views.Term_Edit.Landscape;

namespace Maui.AcademicPortal.MVVM.Views.Term_Edit;

public partial class TermEditPage : ContentPage
{
    private readonly TermEditPortraitViewPage termEditViewPortrait = new();
    private readonly TermEditLandscapeViewPage termEditViewLandscape = new();
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    internal TermEditPage()
    {
        InitializeComponent();
        EventSubscriptions();

        this.Content = termEditViewPortrait;
    }

    public void Dispose()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged -= Current_MainDisplayInfoChanged!;
        this.Loaded -= TermEditPage_Loaded;
    }

    private void EventSubscriptions()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged += Current_MainDisplayInfoChanged!;
        this.Loaded += TermEditPage_Loaded;
    }

    private async void TermEditPage_Loaded(object? sender, EventArgs e)
    {
        ViewActions.ResetProcessTrigger(sender: this);

        await Task.Run(() =>
        {
            // Object Creation
            viewModalState.NewValidatableTermViewModel();

            // Assign Object
            TermViewModel? selectedTermViewModel = viewModalState.SelectableTermViewModel;
            ValidatableTermViewModel? validatableTermViewModel = viewModalState.ValidatableTermViewModel;
            if (selectedTermViewModel is not null &&
                validatableTermViewModel is not null)
            {
                validatableTermViewModel.CopyDataFrom(selectedTermViewModel);

                this.termEditViewPortrait.BindingContext = validatableTermViewModel;
                this.termEditViewLandscape.BindingContext = validatableTermViewModel;
            }
        });
    }

    private void Current_MainDisplayInfoChanged(object sender, DisplayInfoChangedEventArgs e)
    {
        ContentView? currentView = ViewProvider.GetNewOrientedPage(e.DisplayInfo.Orientation,
                                            new ContentView[] { termEditViewPortrait, termEditViewLandscape });

        if (currentView != null)
        {
            this.Content = currentView;
        }
    }
}