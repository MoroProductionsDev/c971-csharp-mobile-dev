using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Term_Add;
using Maui.AcademicPortal.MVVM.Views.Term_Edit;

namespace Maui.AcademicPortal.MVVM.Views.Portal.ContentViews;

public partial class PortalActionsView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public PortalActionsView()
	{
		InitializeComponent();
    }

    private async void AddTermBtn_Clicked(object sender, EventArgs e)
    {
        // UI
        ViewActions.StartProcessTrigger(sender: this, triggeredView: this.addBtn);
        await Navigation.PushModalAsync(new TermAddPage());
    }

    private async void EditTermBtn_Clicked(object sender, EventArgs e)
    {
        SelectableTermViewModel? selectableTermViewModel = viewModalState.SelectableTermViewModel;
        
        if (selectableTermViewModel is not null)
        {
            // UI
            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.editBtn);
            await Navigation.PushModalAsync(new TermEditPage());
        }
        else
        {
            // UI
            App.Current?.MainPage?.DisplayAlert("Unselected Terms", "Please select a term to proceed.", "Ok");
        }
    }

    private async void DeleteTermBtn_Clicked(object sender, EventArgs e)
    {
        SelectableTermViewModel? selectableTermViewModel = viewModalState.SelectableTermViewModel;
        
        if (selectableTermViewModel is null)
        {
            // UI
            App.Current?.MainPage?.DisplayAlert("Unselected Terms", "Please select a term to proceed.", "Ok");
            return;
        }

        //UI
        var result = await App.Current!.MainPage!.DisplayAlert("Delete Term", $"Are you sure you want to delete this term [{selectableTermViewModel.Title}]?", "Yes", "Cancel");

        if (result)
        {
            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.deleteBtn);

            // Logic
            await Task.Run(async () =>
            {
                PortalViewModel mainPortalViewModal = ((App)App.Current!).ViewModelState.PortalViewModel;

                // ** Await Database **
                await mainPortalViewModal.RemoveTerm(selectableTermViewModel.CurrentTerm, selectableTermViewModel.SelectedIdx);
                await viewModalState.PortalViewModel.FetchTerms();
            });

            // UI
            ViewActions.ResetProcessTrigger(sender: this);

            // Must be call as the last one.
            ViewActions.OnSuccesfulTermSubmission?.Invoke(this, EventArgs.Empty);
        }
    }
}