using Maui.AcademicPortal.MVVM.ViewModels;

namespace Maui.AcademicPortal.MVVM.Views.Portal.ContentViews;

public partial class PortalHeaderView : ContentView
{
    private readonly PortalViewModel mainPortalViewModal = ((App)App.Current!).ViewModelState.PortalViewModel;

    public PortalHeaderView()
    {
        InitializeComponent();
        EventSubscriptions();

        this.BindingContext = mainPortalViewModal;
    }
    public void Dispose()
    {
        ViewActions.OnSuccesfulTermSubmission -= UpdateCollectionView;
        this.Loaded -= PortalHeaderView_Loaded;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnSuccesfulTermSubmission += UpdateCollectionView;
        this.Loaded += PortalHeaderView_Loaded;
    }

    private void PortalHeaderView_Loaded(object? sender, EventArgs e)
    {
        UpdateWelcomingLabel();
    }

    private void UpdateCollectionView(object? sender, EventArgs e)
    {
        this.termsCountLbl.Text = $"({mainPortalViewModal.Terms.Count.ToString()}) Term(s)";
    }

    private void UpdateWelcomingLabel()
    {
        const string morningMessage = "Good Morning, Student";
        const string afternoonMessage = "Good Afternoon, Student";
        const string eveningMessage = "Good Evening, Student";
        const string nigthMessage = "Hey Night Owl!";

        if (DateTime.Now.TimeOfDay <= new TimeSpan(6, 0, 0))
        {
            this.portalWelcomeLbl.Text = nigthMessage;
        }
        else if (DateTime.Now.TimeOfDay <= new TimeSpan(12, 0, 0))
        {
            this.portalWelcomeLbl.Text = morningMessage;
        } else if (DateTime.Now.TimeOfDay <= new TimeSpan(17, 0, 0))
        {
            this.portalWelcomeLbl.Text = afternoonMessage;
        } else
        {
            this.portalWelcomeLbl.Text = eveningMessage;
        }
    }
}