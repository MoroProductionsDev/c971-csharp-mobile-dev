using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Term;

namespace Maui.AcademicPortal.MVVM.Views.Portal.ContentViews;

public partial class PortalTermsView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;

    public PortalTermsView()
	{
		InitializeComponent();
        EventSubscriptions();

        var mainPortalView = this.viewModalState.PortalViewModel;
        this.termsViewCollection.ItemsSource = mainPortalView.Terms;
    }

    public void Dispose()
    {
        ViewActions.OnSuccesfulTermSubmission -= UpdateCollectionView;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnSuccesfulTermSubmission += UpdateCollectionView;
    }

    private async void UpdateCollectionView(object? sender, EventArgs e)
    {
        await MainThread.InvokeOnMainThreadAsync(() =>
        {
            var mainPortalViewModal = this.viewModalState.PortalViewModel;
            this.termsViewCollection.ItemsSource = mainPortalViewModal.Terms;
        });
    }

    private async void TermsViewCollection_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        // Object Creation
        await Task.Run(() => { 
            viewModalState.NewSelectableTermViewModel(this.termsViewCollection.ItemsSource.Cast<object>(), this.termsViewCollection.SelectedItem);
        });
        
        await MainThread.InvokeOnMainThreadAsync(() =>
        {
            SelectableTermViewModel? selectableTermViewModel = this.viewModalState.SelectableTermViewModel;
            selectableTermViewModel!.SelectedTerm = this.termsViewCollection.SelectedItem;
        
            // UI
            ViewActions.CVBtn_EnablingDisablingState(this.termsViewCollection, selectableTermViewModel.SelectedIdx);
        });
    }

    private void TermCourses_ExpandedChanged(object sender, CommunityToolkit.Maui.Core.ExpandedChangedEventArgs e)
    {
        SelectableTermViewModel? selectableTermViewModel = this.viewModalState.SelectableTermViewModel;

        if (selectableTermViewModel is not null)
        {
            // UI
            ViewActions.CVBtnContractring_ExpandingState(this.termsViewCollection, 
                                                        selectableTermViewModel.SelectedIdx,
                                                        selectableTermViewModel.GetSelectedItemCoursesCount());
        }
    }

    private async void ViewTermBtn_Clicked(object sender, EventArgs e)
    {
        SelectableTermViewModel? selectableTermViewModel = this.viewModalState.SelectableTermViewModel;

        if (selectableTermViewModel is not null)
        {
            // ** Await Database
            await viewModalState.SelectableTermViewModel!.FetchCourses();

            // UI
            var viewTermBtns = termsViewCollection.GetVisualTreeDescendants().Where(view => view.GetType() == typeof(Button)).ToArray();
            ViewActions.StartProcessTrigger(sender: this, triggeredView: ((Button)viewTermBtns[selectableTermViewModel.SelectedIdx]));
            
            await Navigation.PushAsync(new TermPage());
        }
    }
}

