using Maui.AcademicPortal.MVVM.Views.Portal.Landscape;
using Maui.AcademicPortal.MVVM.Views.Portal.Portrait;

namespace Maui.AcademicPortal.MVVM.Views.Portal;

public partial class PortalPage : ContentPage
{
    private readonly PortalPortraitViewPage portalViewPortrait = new();
    private readonly PortalLandscapeViewPage portalViewLandscape = new();
    public PortalPage()
	{
        InitializeComponent();
        EventSubscriptions();

        this.Content = portalViewPortrait;
    }

    public void Dispose()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged -= Current_MainDisplayInfoChanged!;
        this.Loaded -= PortalPage_Loaded;
    }

    private void EventSubscriptions()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged += Current_MainDisplayInfoChanged!;
        this.Loaded += PortalPage_Loaded;
    }

    private void PortalPage_Loaded(object? sender, EventArgs e)
    {
        ViewActions.ResetProcessTrigger(sender: this);
    }

    private void Current_MainDisplayInfoChanged(object sender, DisplayInfoChangedEventArgs e)
    {
        ContentView? currentView = ViewProvider.GetNewOrientedPage(e.DisplayInfo.Orientation,
                                            new ContentView[] { portalViewPortrait, portalViewLandscape });

        if (currentView != null)
        {
            this.Content = currentView;
        }
    }
}

