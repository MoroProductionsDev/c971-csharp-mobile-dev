﻿using Maui.AcademicPortal.MVVM.ViewModels.Validation;

namespace Maui.AcademicPortal.MVVM.Views.Validation
{
    internal static class UIValidator
    {
        public static void UpdateValidationView<T>(ValidatableObject<T> validatableObject, Label errorLbl)
        {
            if (!validatableObject.IsValid)
            {
                if (validatableObject.Errors.Count() > 0)
                {
                    errorLbl.Text = validatableObject.Errors.FirstOrDefault();
                    errorLbl.IsVisible = true;
                } else if (validatableObject.ComparableErrors.Count() > 0)
                {
                    errorLbl.Text = validatableObject.ComparableErrors.FirstOrDefault();
                    errorLbl.IsVisible = true;
                }
            }
            else
            {
                errorLbl.Text = string.Empty;
                errorLbl.IsVisible = false;
            }
        }
    }
}
