using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Validation;

namespace Maui.AcademicPortal.MVVM.Views.Course_Add.ContentViews;

public partial class CourseAddNoteView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public CourseAddNoteView()
	{
		InitializeComponent();
        EventSubscriptions();
    }

    public void Dispose()
    {
        ViewActions.OnOrientationChanged -= ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission -= ValidateUIComponents!;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnOrientationChanged += ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission += ValidateUIComponents!;
    }

    private void ResumeBindingContext(object sender, EventArgs e)
    {
        // To work the BidingMode must be set to 'TwoWay'.
        this.BindingContext = ((App)App.Current!).ViewModelState.ValidatableCourseViewModel;
    }

    private void NoteTxtBox_TextChanged(object sender, TextChangedEventArgs e)
    {
        var addableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (addableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: addableCourseViewModel.vNote, errorLbl: this.noteErrorLbl);
        }
    }

    private void ValidateUIComponents(object sender, EventArgs e)
    {
        var addableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (addableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: addableCourseViewModel.vNote, errorLbl: this.noteErrorLbl);
        }
    }
}