using Maui.AcademicPortal.MVVM.Models;
using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Validation;

namespace Maui.AcademicPortal.MVVM.Views.Course_Add.ContentViews;

public partial class CourseAddNameAndDatesView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public CourseAddNameAndDatesView()
	{
		InitializeComponent();
        EventSubscriptions();

        // Restrict the DatePicker range to the term start and end dates.
        if (viewModalState.SelectableTermViewModel is not null)
        {
            this.startDatePicker.MinimumDate = viewModalState.SelectableTermViewModel.CurrentTerm.StartDate;
            this.startDatePicker.MaximumDate = viewModalState.SelectableTermViewModel.CurrentTerm.EndDate;
            this.endDatePicker.MinimumDate = viewModalState.SelectableTermViewModel.CurrentTerm.StartDate;
            this.endDatePicker.MaximumDate = viewModalState.SelectableTermViewModel.CurrentTerm.EndDate;
        }
    }

    public void Dispose()
    {
        ViewActions.OnOrientationChanged -= ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission -= ValidateUIComponents!;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnOrientationChanged += ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission += ValidateUIComponents!;
    }

    private void ResumeBindingContext(object sender, EventArgs e)
    {
        // To work the BidingMode must be set to 'TwoWay'.
        this.BindingContext = ((App)App.Current!).ViewModelState.ValidatableCourseViewModel;
    }

    private void CourseStatusPicker_SelectedIndexChanged(object sender, EventArgs e)
    {
        var addableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        const int NO_INDEX_SELECTED = -1;
        if (addableCourseViewModel is not null && this.courseStatusPicker.SelectedIndex != NO_INDEX_SELECTED)
        {
            // Logic
            addableCourseViewModel.vStatus.Value = addableCourseViewModel.CourseStatuses[this.courseStatusPicker.SelectedIndex].Status;
            addableCourseViewModel.vSelectedStatusIdx.Value = this.courseStatusPicker.SelectedIndex;
            
            // UI
            this.statusErrorLbl.Text = String.Empty;
            this.statusErrorLbl.IsVisible = false;
        }
    }

    private void NameTxtField_TextChanged(object sender, TextChangedEventArgs e)
    {
        var addableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (addableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: addableCourseViewModel.vName, errorLbl: this.nameErrorLbl);
        }
    }

    private void StartDatePicker_DateSelected(object sender, DateChangedEventArgs e)
    {
        var addableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (addableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<DateTime>(validatableObject: addableCourseViewModel.vStartDate, errorLbl: this.statusErrorLbl);
        }
    }

    private void EndDatePicker_DateSelected(object sender, DateChangedEventArgs e)
    {
        var addableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (addableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<DateTime>(validatableObject: addableCourseViewModel.vEndDate, errorLbl: this.endDateErrorLbl);
        }
    }

    private void ValidateUIComponents(object sender, EventArgs e)
    {
        var addableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (addableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: addableCourseViewModel.vName, errorLbl: this.nameErrorLbl);
            UIValidator.UpdateValidationView<DateTime>(validatableObject: addableCourseViewModel.vStartDate, errorLbl: this.statusErrorLbl);
            UIValidator.UpdateValidationView<DateTime>(validatableObject: addableCourseViewModel.vEndDate, errorLbl: this.endDateErrorLbl);
            UIValidator.UpdateValidationView<int>(validatableObject: addableCourseViewModel.vSelectedStatusIdx, errorLbl: this.statusErrorLbl);
        }
    }
}