using Maui.AcademicPortal.MVVM.ViewModels;

namespace Maui.AcademicPortal.MVVM.Views.Course_Add.ContentViews;

public partial class CourseAddActionView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public CourseAddActionView()
	{
		InitializeComponent();
	}

    private async void AddBtn_Clicked(object sender, EventArgs e)
    {
        // PROCESS AND VALIDATION
        // Logic
        SelectableTermViewModel? selectedTermViewModel = viewModalState.SelectableTermViewModel;
        ValidatableCourseViewModel? validatableCourseViewModel = viewModalState.ValidatableCourseViewModel;

        if (selectedTermViewModel is not null &&
            validatableCourseViewModel is not null &&
            validatableCourseViewModel.Validate())
        {
            // UI
            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.addBtn);

            await Task.Run(async () =>
            {
                // Logic
                CourseViewModel newAddableCourseViewModel = viewModalState.NewCourseViewModel();
                validatableCourseViewModel.CopyDataTo(newAddableCourseViewModel);

                // ** Await Database **
                await selectedTermViewModel.AddCourse(newAddableCourseViewModel.CurrentCourse);
                await selectedTermViewModel.FetchCourses();
            });

            // UI
            await Navigation.PopModalAsync();
            ViewActions.ResetProcessTrigger(sender: this);

            // Must be call as the last one.
            ViewActions.OnSuccesfulCourseSubmission?.Invoke(this, EventArgs.Empty);
        } else
        {
            ViewActions.OnAttemptedSubmission?.Invoke(this, EventArgs.Empty);
        }
    }

    private async void CancelBtn_Clicked(object sender, EventArgs e)
    {
        ViewActions.StartProcessTrigger(sender: this, triggeredView: this.cancelBtn);
        await Navigation.PopModalAsync();
        ViewActions.ResetProcessTrigger(sender: this);
    }
}