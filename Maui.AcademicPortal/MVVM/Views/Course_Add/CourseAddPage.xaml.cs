using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Course_Add.Portrait;
using Maui.AcademicPortal.MVVM.Views.Course_Add.Landscape;

namespace Maui.AcademicPortal.MVVM.Views.Course_Add;

public partial class CourseAddPage : ContentPage
{
    private readonly CourseAddPortraitViewPage courseEditViewPortrait = new();
    private readonly CourseAddLandscapeViewPage courseEditViewLandscape = new();
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;

    internal CourseAddPage()
	{
		InitializeComponent();
        EventSubscriptions();

        this.Content = courseEditViewPortrait;
    }
    public void Dispose()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged -= Current_MainDisplayInfoChanged!;
        this.Loaded -= CourseAddPage_Loaded;
    }

    private void EventSubscriptions()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged += Current_MainDisplayInfoChanged!;
        this.Loaded += CourseAddPage_Loaded; ;
    }

    private async void CourseAddPage_Loaded(object? sender, EventArgs e)
    {
        ViewActions.ResetProcessTrigger(sender: this);

        await Task.Run(() =>
        {
            // Object Creation
            viewModalState.NewValidatableCourseViewModel();

            // Assign Object
            ValidatableCourseViewModel? validatableCourseViewModel = viewModalState.ValidatableCourseViewModel;
            this.courseEditViewPortrait.BindingContext = validatableCourseViewModel;
            this.courseEditViewLandscape.BindingContext = validatableCourseViewModel;
        });
    }

    private void Current_MainDisplayInfoChanged(object sender, DisplayInfoChangedEventArgs e)
    {
        ViewActions.OnOrientationChanged?.Invoke(this, EventArgs.Empty);
        ContentView? currentView = ViewProvider.GetNewOrientedPage(e.DisplayInfo.Orientation,
                                            new ContentView[] { courseEditViewPortrait, courseEditViewLandscape });

        if (currentView != null)
        {
            this.Content = currentView;
        }
    }
}