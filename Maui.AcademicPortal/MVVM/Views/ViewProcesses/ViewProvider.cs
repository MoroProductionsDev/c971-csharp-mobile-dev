﻿namespace Maui.AcademicPortal.MVVM.Views
{
    internal static class ViewProvider
    {
        public static ContentView? GetNewOrientedPage(DisplayOrientation orientation, ContentView[] views)
        {
            ContentView? contentView = null;
            if (DisplayOrientation.Portrait == orientation)
            {
                contentView = views[0];
            } else if (DisplayOrientation.Landscape == orientation)
            {
                contentView = views[1];
            }

            return contentView;
        }
    }
}
