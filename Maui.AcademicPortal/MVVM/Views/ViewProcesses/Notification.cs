﻿using Maui.AcademicPortal.MVVM.ViewModels.Validation;
using Plugin.LocalNotification;

namespace Maui.AcademicPortal.MVVM.Views.ViewProcesses
{
    internal static class Notification
    {
        public static async Task<bool> RunDateNotication(int id, string title, DateTime startDate, DateTime endDate)
        {
            bool sendPush = false;
            string description = string.Empty;

            if (ValueValidator.CheckingForTodaysDate(startDate))
            {
                description += $"Start date: {startDate.ToShortDateString()}";
                sendPush = true;
            }
            if (ValueValidator.CheckingForTodaysDate(endDate))
            {

                description += description.Equals(String.Empty) ?
                    $"End date: {endDate.ToShortDateString()}" :
                    $"\nEnd date:  {endDate.ToShortDateString()}";

                sendPush = true;
            }

            if (sendPush)
            {
                var notification = new NotificationRequest
                {
                    NotificationId = id,
                    Title = title,
                    Description = description,
                    Schedule =
                        {
                            NotifyTime = DateTime.Now.AddSeconds(2),
                            NotifyAutoCancelTime = DateTime.Now.AddSeconds(15)
                        }
                };

                return await LocalNotificationCenter.Current.Show(notification);
            }
            return sendPush;
        }
    }
}
