﻿using CommunityToolkit.Maui.Views;

namespace Maui.AcademicPortal.MVVM.Views
{
    internal static class ViewActions
    {
        internal static EventHandler<UIProcessEventArgs>? OnUIProcess;
        internal static EventHandler? OnOrientationChanged;
        internal static EventHandler? OnSuccesfulTermSubmission;
        internal static EventHandler? OnSuccesfulCourseSubmission;
        internal static EventHandler? OnAttemptedSubmission;

        internal static UIProcessEventArgs? UIProcessEventArgs { get; set; }
        const int INDEX_NOT_FOUND = -1;

        // CollectionViewButton_EnablingDisablingState
        public static void CVBtn_EnablingDisablingState(CollectionView activeCollectionView, int selectedIdx)
        {
            var viewBtnViews = activeCollectionView.GetVisualTreeDescendants().Where(view => view.GetType() == typeof(Button)).ToArray();
            

            if (viewBtnViews == null || viewBtnViews.Length == 0 ||
                selectedIdx == INDEX_NOT_FOUND)
            {
                return;
            }

            for (int idx = 0; idx < viewBtnViews.Length; ++idx)
            {
                if (idx != selectedIdx)
                {
                    ((Button)viewBtnViews[idx]).IsEnabled = false;
                }
                else
                {
                    ((Button)viewBtnViews[idx]).IsEnabled = true;
                }
            }
        }

        public static void StartProcessTrigger(object sender, View triggeredView)
        {
            UIProcessEventArgs = new UIProcessEventArgs()
            {
                IsProcessing = true,
                View = triggeredView
            };

            OnUIProcess?.Invoke(sender, UIProcessEventArgs);
        }

        public static void ResetProcessTrigger(object sender)
        {
            if (UIProcessEventArgs is not null)
            {
                UIProcessEventArgs.IsProcessing = false;
                OnUIProcess?.Invoke(sender, UIProcessEventArgs);
            } else
            {
                UIProcessEventArgs = new UIProcessEventArgs()
                {
                    IsProcessing = false,
                    View = null // no view to disable/enable
                };

                OnUIProcess?.Invoke(sender, UIProcessEventArgs);
            }
        }

        public static void CVBtnContractring_ExpandingState(CollectionView activateCollectionView, int selectedIdx, int childrenCount)
        {
            const int ROW_HEIGHT = 135;
            var childrenExpanderViews = activateCollectionView.GetVisualTreeDescendants().Where(view => view.GetType() == typeof(Expander)).ToArray();
            var expandedBorderViews = activateCollectionView.GetVisualTreeDescendants().Where(view => view.GetType() == typeof(Border)).ToArray();
   

            if (childrenExpanderViews != null && childrenExpanderViews.Length != 0 &&
                expandedBorderViews != null && expandedBorderViews.Length != 0 &&
                selectedIdx != INDEX_NOT_FOUND)
            {
                if (((Expander)childrenExpanderViews[selectedIdx]).IsExpanded)
                {
                    if (childrenCount >= 0)
                    {
                        ((Expander)childrenExpanderViews[selectedIdx]).WidthRequest = 345;
                        ((Border)expandedBorderViews[selectedIdx]).HeightRequest = childrenCount * ROW_HEIGHT;
                    }
                }
                else
                {
                    ((Expander)childrenExpanderViews[selectedIdx]).WidthRequest = 25;
                    ((Border)expandedBorderViews[selectedIdx]).HeightRequest = 100;
                }
            }
        }
    }

    internal class UIProcessEventArgs : EventArgs
    {
        internal bool IsProcessing { get; set; }
        private View? _view;
        internal View? View
        {
            get => _view;
            set
            {
                if (value is not null)
                {
                    _view = value;
                    _view.IsEnabled = !IsProcessing;
                }
            }
        }
    }
}
