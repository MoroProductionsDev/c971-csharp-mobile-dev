namespace Maui.AcademicPortal.MVVM.Views.SharedViews;

public partial class SharedActivityIndicatorView : ContentView
{
	public SharedActivityIndicatorView()
	{
		InitializeComponent();
        EventSubscriptions();
    }

    public void Dispose()
    {
        ViewActions.OnUIProcess -= ActivityIndactor_Display!;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnUIProcess += ActivityIndactor_Display!;
    }

    private void ActivityIndactor_Display(object sender, UIProcessEventArgs e)
    {
        this.activityIndicator.IsRunning = e.IsProcessing;

        if (e.View is not null)
        {
            e.View.IsEnabled = !e.IsProcessing;
        }
    }
}