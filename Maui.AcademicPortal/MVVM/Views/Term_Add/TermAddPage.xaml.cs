using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Term_Add.Portrait;
using Maui.AcademicPortal.MVVM.Views.Term_Add.Landscape;

namespace Maui.AcademicPortal.MVVM.Views.Term_Add;

public partial class TermAddPage : ContentPage
{
    private readonly TermAddPortraitViewPage termAddViewPortrait = new();
    private readonly TermAddLandscapeViewPage termAddViewLandscape = new();
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    internal TermAddPage()
    {
        InitializeComponent();
        EventSubscriptions();

        this.Content = termAddViewPortrait;
    }

    public void Dispose()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged -= Current_MainDisplayInfoChanged!;
        this.Loaded -= TermAddPage_Loaded;
    }

    private void EventSubscriptions()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged += Current_MainDisplayInfoChanged!;
        this.Loaded += TermAddPage_Loaded;
    }

    private async void TermAddPage_Loaded(object? sender, EventArgs e)
    {
        ViewActions.ResetProcessTrigger(sender: this);

        await Task.Run(() =>
        {
            // Object Creation
            viewModalState.NewValidatableTermViewModel();

            // Assign Object
            ValidatableTermViewModel? validatableTermViewModel = viewModalState.ValidatableTermViewModel;
            this.termAddViewPortrait.BindingContext = validatableTermViewModel;
            this.termAddViewLandscape.BindingContext = validatableTermViewModel;
        });
    }

    private void Current_MainDisplayInfoChanged(object sender, DisplayInfoChangedEventArgs e)
    {
        ViewActions.OnOrientationChanged?.Invoke(this, EventArgs.Empty);
        ContentView? currentView = ViewProvider.GetNewOrientedPage(e.DisplayInfo.Orientation,
                                            new ContentView[] { termAddViewPortrait, termAddViewLandscape });
        if (currentView != null)
        {
            this.Content = currentView;
        }
    }
}
