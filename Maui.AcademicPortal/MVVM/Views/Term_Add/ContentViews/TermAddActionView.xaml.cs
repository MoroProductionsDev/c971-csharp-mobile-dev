using Maui.AcademicPortal.MVVM.ViewModels;

namespace Maui.AcademicPortal.MVVM.Views.Term_Add.ContentViews;

public partial class TermAddActionView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public TermAddActionView()
	{
		InitializeComponent();
    }

    private async void AddBtn_Clicked(object sender, EventArgs e)
    {
        // PROCESS AND VALIDATION
        // Logic
        PortalViewModel? mainPortalViewModel = viewModalState.PortalViewModel;
        ValidatableTermViewModel? ValidatableTermViewModel = viewModalState.ValidatableTermViewModel;

        if (mainPortalViewModel is not null &&
            ValidatableTermViewModel is not null &&
            ValidatableTermViewModel.Validate())
        {
            // UI
            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.addBtn);

            // Logic
            await Task.Run(async () =>
            {
                TermViewModel newAddableTermViewModel = viewModalState.NewTermViewModel();
                ValidatableTermViewModel.CopyDataTo(newAddableTermViewModel);

                // ** Await Database **
                await mainPortalViewModel.AddTerm(newAddableTermViewModel.CurrentTerm);
                await mainPortalViewModel.FetchTerms();
            });
         
            await Navigation.PopModalAsync();
            ViewActions.ResetProcessTrigger(sender: this);

            // Must be call as the last one.
            ViewActions.OnSuccesfulTermSubmission?.Invoke(this, EventArgs.Empty);
        } else
        {
            ViewActions.OnAttemptedSubmission?.Invoke(this, EventArgs.Empty);
        }
    }

    private async void CancelBtn_Clicked(object sender, EventArgs e)
    {
        ViewActions.StartProcessTrigger(sender: this, triggeredView: this.cancelBtn);
        await Navigation.PopModalAsync();
        ViewActions.ResetProcessTrigger(sender: this);
    }
}