using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Course_Edit.Portrait;
using Maui.AcademicPortal.MVVM.Views.Course_Edit.Landscape;

namespace Maui.AcademicPortal.MVVM.Views.Course_Edit;

public partial class CourseEditPage : ContentPage
{
    private readonly CourseEditPortraitViewPage courseEditViewPortrait = new();
    private readonly CourseEditLandscapeViewPage courseEditViewLandscape = new();
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;

    internal CourseEditPage()
	{
		InitializeComponent();
        EventSubscriptions();

        this.Content = courseEditViewPortrait;
    }
    public void Dispose()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged -= Current_MainDisplayInfoChanged!;
        this.Loaded -= CourseEditPage_Loaded;
    }

    private void EventSubscriptions()
    {
        DeviceDisplay.Current.MainDisplayInfoChanged += Current_MainDisplayInfoChanged!;
        this.Loaded += CourseEditPage_Loaded;
    }

    private async void CourseEditPage_Loaded(object? sender, EventArgs e)
    {
        ViewActions.ResetProcessTrigger(sender: this);

        await Task.Run(() =>
        {
            // Object Creation
            viewModalState.NewValidatableCourseViewModel();

            // Assign Object
            SelectableCourseViewModel? selectedCourseViewModel = viewModalState.SelectableCourseViewModel;
            ValidatableCourseViewModel? validatableCourseViewModel = viewModalState.ValidatableCourseViewModel;
            if (selectedCourseViewModel is not null &&
                validatableCourseViewModel is not null)
            {
                validatableCourseViewModel.CopyDataFrom(selectedCourseViewModel);

                this.courseEditViewPortrait.BindingContext = validatableCourseViewModel;
                this.courseEditViewLandscape.BindingContext = validatableCourseViewModel;
            }
        });
    }

    private void Current_MainDisplayInfoChanged(object sender, DisplayInfoChangedEventArgs e)
    {
        ViewActions.OnOrientationChanged?.Invoke(this, EventArgs.Empty);
        ContentView? currentView = ViewProvider.GetNewOrientedPage(e.DisplayInfo.Orientation,
                                            new ContentView[] { courseEditViewPortrait, courseEditViewLandscape });

        if (currentView != null)
        {
            this.Content = currentView;
        }
    }
}