using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Validation;

namespace Maui.AcademicPortal.MVVM.Views.Course_Edit.ContentViews;

public partial class CourseEditAssementsView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public CourseEditAssementsView()
	{
		InitializeComponent();
        EventSubscriptions();

        // Restrict the DatePicker range to the term start and end dates.
        if (viewModalState.SelectableTermViewModel is not null)
        {
            this.objectiveAssessmentStartDatePicker.MinimumDate = viewModalState.SelectableTermViewModel.CurrentTerm.StartDate;
            this.objectiveAssessmentStartDatePicker.MaximumDate = viewModalState.SelectableTermViewModel.CurrentTerm.EndDate;
            this.objectiveAssessmentEndDatePicker.MinimumDate = viewModalState.SelectableTermViewModel.CurrentTerm.StartDate;
            this.objectiveAssessmentEndDatePicker.MaximumDate = viewModalState.SelectableTermViewModel.CurrentTerm.EndDate;

            this.performanceAssessmentStartDatePicker.MinimumDate = viewModalState.SelectableTermViewModel.CurrentTerm.StartDate;
            this.performanceAssessmentStartDatePicker.MaximumDate = viewModalState.SelectableTermViewModel.CurrentTerm.EndDate;
            this.performanceAssessmentEndDatePicker.MinimumDate = viewModalState.SelectableTermViewModel.CurrentTerm.StartDate;
            this.performanceAssessmentEndDatePicker.MaximumDate = viewModalState.SelectableTermViewModel.CurrentTerm.EndDate;
        }
    }

    public void Dispose()
    {
        ViewActions.OnOrientationChanged -= ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission -= ValidateUIComponents!;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnOrientationChanged += ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission += ValidateUIComponents!;
    }

    private void ResumeBindingContext(object sender, EventArgs e)
    {
        // To work the BidingMode must be set to 'TwoWay'.
        this.BindingContext = ((App)App.Current!).ViewModelState.ValidatableCourseViewModel;
    }

    private void ObjectiveAssessmentNameTxtField_TextChanged(object sender, TextChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vObjectiveAssessmentName, errorLbl: this.objectiveAssessmentNameErrorLbl);
        }
    }

    private void ObjectiveAssessmentStartDatePicker_DateSelected(object sender, DateChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vObjectiveAssessmentStartDate, errorLbl: this.objectiveAssessmentStartDateErrorLbl);
        }
    }

    private void ObjectiveAssessmentEndDatePicker_DateSelected(object sender, DateChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vObjectiveAssessmentEndDate, errorLbl: this.objectiveAssessmentEndDateErrorLbl);
        }
    }

    private void PerformanceAssessmentNameTxtField_TextChanged(object sender, TextChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vPerformanceAssessmentName, errorLbl: this.performanceAssessmentNameErrorLbl);
        }
    }

    private void PerformanceAssessmentStartDatePicker_DateSelected(object sender, DateChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vPerformanceAssessmentStartDate, errorLbl: this.performanceAssessmentStartDateErrorLbl);
        }
    }

    private void PerformanceAssessmentEndDatePicker_DateSelected(object sender, DateChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vPerformanceAssessmentEndDate, errorLbl: this.performanceAssessmentEndDateErrorLbl);
        }
    }

    private void ValidateUIComponents(object sender, EventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vObjectiveAssessmentName, errorLbl: this.objectiveAssessmentNameErrorLbl);
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vObjectiveAssessmentStartDate, errorLbl: this.objectiveAssessmentStartDateErrorLbl);
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vObjectiveAssessmentEndDate, errorLbl: this.objectiveAssessmentEndDateErrorLbl);
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vPerformanceAssessmentName, errorLbl: this.performanceAssessmentNameErrorLbl);
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vPerformanceAssessmentStartDate, errorLbl: this.performanceAssessmentStartDateErrorLbl);
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vPerformanceAssessmentEndDate, errorLbl: this.performanceAssessmentEndDateErrorLbl);
        }
    }
}