using Maui.AcademicPortal.MVVM.ViewModels;

namespace Maui.AcademicPortal.MVVM.Views.Course_Edit.ContentViews;

public partial class CourseEditActionView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public CourseEditActionView()
	{
		InitializeComponent();
	}

    private async void UpdateBtn_Clicked(object sender, EventArgs e)
    {
        // PROCESS AND VALIDATION
        // Logic
        SelectableTermViewModel? selectedTermViewModel = viewModalState.SelectableTermViewModel;
        ValidatableCourseViewModel? validatableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        CourseViewModel? selectableCourseViewModel = viewModalState.SelectableCourseViewModel;

        if (selectedTermViewModel is not null &&
            validatableCourseViewModel is not null &&
            selectableCourseViewModel is not null &&
            validatableCourseViewModel.CheckIfDataChange(selectableCourseViewModel) &&
            validatableCourseViewModel.Validate())
        {
            // UI
            ViewActions.StartProcessTrigger(sender: this, triggeredView: this.updateBtn);

            // Logic
            await Task.Run(async () =>
            {
                validatableCourseViewModel.CopyDataTo(selectableCourseViewModel);
                        
                int selectedIdx = ((SelectableCourseViewModel) selectableCourseViewModel).SelectedIdx;

                // ** Await Database **
                await selectedTermViewModel.EditCourse(selectableCourseViewModel.CurrentCourse, selectedIdx);
                await selectedTermViewModel.FetchCourses();
            });

            // UI
            await Navigation.PopModalAsync();
            ViewActions.ResetProcessTrigger(sender: this);

            // Must be call as the last one.
            ViewActions.OnSuccesfulCourseSubmission?.Invoke(this, EventArgs.Empty);
        }
        else
        {
            if (validatableCourseViewModel is not null && !validatableCourseViewModel.HasDataChanged)
            {
                App.Current?.MainPage?.DisplayAlert("Unchanged Data", "Data values has not changed.", "Ok");
            }
            else
            {
                ViewActions.OnAttemptedSubmission?.Invoke(this, EventArgs.Empty);
            }
        }
    }

    private async void CancelBtn_Clicked(object sender, EventArgs e)
    {
        ViewActions.StartProcessTrigger(sender: this, triggeredView: this.cancelBtn);
        await Navigation.PopModalAsync();
        ViewActions.ResetProcessTrigger(sender: this);
    }
}