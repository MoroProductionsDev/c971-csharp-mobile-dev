using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Validation;

namespace Maui.AcademicPortal.MVVM.Views.Course_Edit.ContentViews;

public partial class CourseEditInstructorView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public CourseEditInstructorView()
	{
		InitializeComponent();
        EventSubscriptions();
    }

    public void Dispose()
    {
        ViewActions.OnOrientationChanged -= ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission -= ValidateUIComponents!;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnOrientationChanged += ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission += ValidateUIComponents!;
    }

    private void ResumeBindingContext(object sender, EventArgs e)
    {
        // To work the BidingMode must be set to 'TwoWay'.
        this.BindingContext = ((App)App.Current!).ViewModelState.ValidatableCourseViewModel;
    }

    private void InstructorNameTxtfield_TextChanged(object sender, TextChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vInstructorName, errorLbl: this.instructorNameErrorLbl);
        }
    }

    private void InstructorPhoneTxtfield_TextChanged(object sender, TextChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vInstructorPhone, errorLbl: this.instructorPhoneErrorLbl);
        }
    }

    private void InstructorEmailTxtfield_TextChanged(object sender, TextChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vInstructorEmail, errorLbl: this.instructorEmailErrorLbl);
        }
    }

    private void ValidateUIComponents(object sender, EventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vInstructorName, errorLbl: this.instructorNameErrorLbl);
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vInstructorPhone, errorLbl: this.instructorPhoneErrorLbl);
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vInstructorEmail, errorLbl: this.instructorEmailErrorLbl);
        }
    }
}