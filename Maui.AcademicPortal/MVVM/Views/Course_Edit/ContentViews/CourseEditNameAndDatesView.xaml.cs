using Maui.AcademicPortal.MVVM.Models;
using Maui.AcademicPortal.MVVM.ViewModels;
using Maui.AcademicPortal.MVVM.Views.Validation;

namespace Maui.AcademicPortal.MVVM.Views.Course_Edit.ContentViews;

public partial class CourseEditNameAndDatesView : ContentView
{
    private readonly ViewModelState viewModalState = ((App)App.Current!).ViewModelState;
    public CourseEditNameAndDatesView()
	{
		InitializeComponent();
        EventSubscriptions();

        // Restrict the DatePicker range to the term start and end dates.
        if (viewModalState.SelectableTermViewModel is not null)
        {
            this.startDatePicker.MinimumDate = viewModalState.SelectableTermViewModel.CurrentTerm.StartDate;
            this.startDatePicker.MaximumDate = viewModalState.SelectableTermViewModel.CurrentTerm.EndDate;
            this.endDatePicker.MinimumDate = viewModalState.SelectableTermViewModel.CurrentTerm.StartDate;
            this.endDatePicker.MaximumDate = viewModalState.SelectableTermViewModel.CurrentTerm.EndDate;
        }
    }

    public void Dispose()
    {
        ViewActions.OnOrientationChanged -= ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission -= ValidateUIComponents!;
        this.courseStatusPicker.BindingContextChanged -= CourseStatusPicker_BindingContextChanged;
    }

    private void EventSubscriptions()
    {
        ViewActions.OnOrientationChanged += ResumeBindingContext!;
        ViewActions.OnAttemptedSubmission += ValidateUIComponents!;
        this.courseStatusPicker.BindingContextChanged += CourseStatusPicker_BindingContextChanged;
    }

    private void CourseStatusPicker_BindingContextChanged(object? sender, EventArgs e)
    {
        ValidatableCourseViewModel? validatableCourseViewModel = viewModalState.ValidatableCourseViewModel;

        if (validatableCourseViewModel is not null)
        {
            CourseStatusEnum selectedStatus = validatableCourseViewModel.vStatus.Value;
            CourseStatus selectableCourseStatus = new CourseStatus(selectedStatus);
            int selectedStatusIdx = selectableCourseStatus.Idx;

            this.courseStatusPicker.SelectedIndex = selectedStatusIdx;
        }
    }

    private void ResumeBindingContext(object sender, EventArgs e)
    {
        // To work the BidingMode must be set to 'TwoWay'.
        this.BindingContext = ((App)App.Current!).ViewModelState.ValidatableCourseViewModel;
    }

    private void CourseStatusPicker_SelectedIndexChanged(object sender, EventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        const int NO_INDEX_SELECTED = -1;
        if (editableCourseViewModel is not null && this.courseStatusPicker.SelectedIndex != NO_INDEX_SELECTED)
        {
            editableCourseViewModel.vStatus.Value = editableCourseViewModel.CourseStatuses[this.courseStatusPicker.SelectedIndex].Status;
            editableCourseViewModel.vSelectedStatusIdx.Value = this.courseStatusPicker.SelectedIndex;
        }
    }

    private void NameTxtField_TextChanged(object sender, TextChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vName, errorLbl: this.nameErrorLbl);
        }
    }

    private void StartDatePicker_DateSelected(object sender, DateChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vStartDate, errorLbl: this.statusErrorLbl);
        }
    }

    private void EndDatePicker_DateSelected(object sender, DateChangedEventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vEndDate, errorLbl: this.endErrorLbl);
        }
    }

    private void ValidateUIComponents(object sender, EventArgs e)
    {
        var editableCourseViewModel = viewModalState.ValidatableCourseViewModel;
        if (editableCourseViewModel is not null)
        {
            UIValidator.UpdateValidationView<string>(validatableObject: editableCourseViewModel.vName, errorLbl: this.nameErrorLbl);
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vStartDate, errorLbl: this.statusErrorLbl);
            UIValidator.UpdateValidationView<DateTime>(validatableObject: editableCourseViewModel.vEndDate, errorLbl: this.endErrorLbl);
            UIValidator.UpdateValidationView<int>(validatableObject: editableCourseViewModel.vSelectedStatusIdx, errorLbl: this.statusErrorLbl);
        }
    }
}