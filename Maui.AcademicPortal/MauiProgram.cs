﻿using CommunityToolkit.Maui;
using Maui.AcademicPortal.MVVM.Models.Storage;
using Microsoft.Extensions.Logging;
using Plugin.LocalNotification;

namespace Maui.AcademicPortal
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .UseMauiCommunityToolkit()
                .UseLocalNotification()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });
            builder.Services.AddSingleton<LocalDbService>();
#if DEBUG
            builder.Logging.AddDebug();
#endif

            return builder.Build();

        }
    }
}
